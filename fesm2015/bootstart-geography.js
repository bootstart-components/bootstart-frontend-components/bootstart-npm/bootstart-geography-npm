import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Component, Input, Directive, ElementRef, EventEmitter, Output, NgModule } from '@angular/core';
import { MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartLocationFormGroup extends FormGroup {
    /**
     * @param {?=} validatorOrOpts
     * @param {?=} asyncValidator
     */
    constructor(validatorOrOpts, asyncValidator) {
        /** @type {?} */
        const builder = new FormBuilder();
        /** @type {?} */
        const controls = {
            city: builder.control(''),
            department: builder.control(''),
            region: builder.control(''),
            country: builder.control(''),
            address: builder.control(''),
            lat: builder.control(0),
            lon: builder.control(0),
        };
        super(controls, validatorOrOpts, asyncValidator);
        this.controls = controls;
    }
    /**
     * Form controls loader.
     * @param {?} geographicLocation Object that contains information about the geographic location.
     * @return {?}
     */
    loadControls(geographicLocation) {
        this.controls.city.setValue(geographicLocation.city);
        this.controls.department.setValue(geographicLocation.department);
        this.controls.region.setValue(geographicLocation.region);
        this.controls.country.setValue(geographicLocation.country);
        this.controls.address.setValue(geographicLocation.address);
        if (geographicLocation.coordinates) {
            this.controls.lat.setValue(geographicLocation.coordinates.lat);
            this.controls.lon.setValue(geographicLocation.coordinates.lon);
        }
    }
    /**
     * Form controls saver into a geographic location.
     * @return {?}
     */
    saveControls() {
        return {
            city: this.controls.city.value,
            department: this.controls.department.value,
            region: this.controls.region.value,
            country: this.controls.country.value,
            address: this.controls.address.value,
            coordinates: {
                lat: this.controls.lat.value,
                lon: this.controls.lon.value
            }
        };
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartLocationFormComponent {
    constructor() {
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._setDefaultOptions();
    }
    /**
     * @return {?}
     */
    _setDefaultOptions() {
        if (this.options.width === undefined) {
            this.options.width = 100;
        }
    }
    /**
     * Place update
     * @param {?} place
     * @return {?}
     */
    updatePlace(place) {
        this.group.controls.city.setValue('');
        this.group.controls.department.setValue('');
        this.group.controls.region.setValue('');
        this.group.controls.country.setValue('');
        place.address_components.forEach(address_component => {
            if (address_component.types.indexOf('locality') >= 0) {
                this.group.controls.city.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('administrative_area_level_2') >= 0) {
                this.group.controls.department.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('administrative_area_level_1') >= 0) {
                this.group.controls.region.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('country') >= 0) {
                this.group.controls.country.setValue(address_component.long_name);
            }
        });
        this.group.controls.lat.setValue(place.geometry.location.lat());
        this.group.controls.lon.setValue(place.geometry.location.lng());
        this.group.controls.address.setValue(place.formatted_address);
    }
}
BootstartLocationFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-location-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{ options.label | translate}}</span>
  </mat-label>

  <input matInput appGooglePlaces
         type="text"
         [placeholder]="this.group.controls.address.value"
         (selectionChange)="updatePlace($event)"
         [required]="options.required">
</mat-form-field>
`
            },] },
];
/** @nocollapse */
BootstartLocationFormComponent.ctorParameters = () => [];
BootstartLocationFormComponent.propDecorators = {
    group: [{ type: Input }],
    options: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class GooglePlacesDirective {
    /**
     * @param {?} elRef
     */
    constructor(elRef) {
        this.elRef = elRef;
        this.selectionChange = new EventEmitter();
        this.element = elRef.nativeElement;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const autocomplete = new google.maps.places.Autocomplete(this.element);
        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            this.selectionChange.emit(autocomplete.getPlace());
        });
    }
}
GooglePlacesDirective.decorators = [
    { type: Directive, args: [{
                selector: '[appGooglePlaces]'
            },] },
];
/** @nocollapse */
GooglePlacesDirective.ctorParameters = () => [
    { type: ElementRef }
];
GooglePlacesDirective.propDecorators = {
    selectionChange: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
class BootstartGeographyModule {
}
BootstartGeographyModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MatFormFieldModule,
                    MatInputModule,
                    MatIconModule,
                    TranslateModule
                ],
                declarations: [
                    BootstartLocationFormComponent,
                    GooglePlacesDirective
                ],
                exports: [
                    BootstartLocationFormComponent,
                    GooglePlacesDirective
                ],
                providers: [],
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */

export { BootstartGeographyModule, BootstartLocationFormGroup, BootstartLocationFormComponent as ɵa, GooglePlacesDirective as ɵb };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWdlb2dyYXBoeS5qcy5tYXAiLCJzb3VyY2VzIjpbIm5nOi8vYm9vdHN0YXJ0LWdlb2dyYXBoeS9saWIvbW9kZWxzL2Zvcm0vYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0tZ3JvdXAudHMiLCJuZzovL2Jvb3RzdGFydC1nZW9ncmFwaHkvbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0vYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZ2VvZ3JhcGh5L2xpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtL2dvb2dsZS1wbGFjZXMvZ29vZ2xlLXBsYWNlcy5kaXJlY3RpdmUudHMiLCJuZzovL2Jvb3RzdGFydC1nZW9ncmFwaHkvbGliL2Jvb3RzdGFydC1nZW9ncmFwaHkubW9kdWxlLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIEFic3RyYWN0Q29udHJvbE9wdGlvbnMsXG4gIEFzeW5jVmFsaWRhdG9yRm4sXG4gIEZvcm1CdWlsZGVyLFxuICBGb3JtQ29udHJvbCxcbiAgRm9ybUdyb3VwLFxuICBWYWxpZGF0b3JGblxufSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0dlb2dyYXBoaWNMb2NhdGlvbn0gZnJvbSAnLi4vbG9jYXRpb24vZ2VvZ3JhcGhpYy1sb2NhdGlvbic7XG5cbmV4cG9ydCBjbGFzcyBCb290c3RhcnRMb2NhdGlvbkZvcm1Hcm91cCBleHRlbmRzIEZvcm1Hcm91cCB7XG5cbiAgY29uc3RydWN0b3IodmFsaWRhdG9yT3JPcHRzPzogVmFsaWRhdG9yRm4gfCBWYWxpZGF0b3JGbltdIHwgQWJzdHJhY3RDb250cm9sT3B0aW9ucyB8IGFueSxcbiAgICAgICAgICAgICAgYXN5bmNWYWxpZGF0b3I/OiBBc3luY1ZhbGlkYXRvckZuIHwgQXN5bmNWYWxpZGF0b3JGbltdIHwgbnVsbCkge1xuICAgIGNvbnN0IGJ1aWxkZXIgPSBuZXcgRm9ybUJ1aWxkZXIoKTtcbiAgICBjb25zdCBjb250cm9scyA9IHtcbiAgICAgIGNpdHkgICAgICA6IGJ1aWxkZXIuY29udHJvbCgnJyksXG4gICAgICBkZXBhcnRtZW50OiBidWlsZGVyLmNvbnRyb2woJycpLFxuICAgICAgcmVnaW9uICAgIDogYnVpbGRlci5jb250cm9sKCcnKSxcbiAgICAgIGNvdW50cnkgICA6IGJ1aWxkZXIuY29udHJvbCgnJyksXG4gICAgICBhZGRyZXNzICAgOiBidWlsZGVyLmNvbnRyb2woJycpLFxuICAgICAgbGF0ICAgICAgIDogYnVpbGRlci5jb250cm9sKDApLFxuICAgICAgbG9uICAgICAgIDogYnVpbGRlci5jb250cm9sKDApLFxuICAgIH07XG4gICAgc3VwZXIoY29udHJvbHMsIHZhbGlkYXRvck9yT3B0cywgYXN5bmNWYWxpZGF0b3IpO1xuICAgIHRoaXMuY29udHJvbHMgPSBjb250cm9scztcbiAgfVxuXG4gIC8qKiBGb3JtIGNvbnRyb2xzICovXG4gIGNvbnRyb2xzOiB7XG4gICAgY2l0eTogRm9ybUNvbnRyb2wsXG4gICAgZGVwYXJ0bWVudDogRm9ybUNvbnRyb2wsXG4gICAgcmVnaW9uOiBGb3JtQ29udHJvbCxcbiAgICBjb3VudHJ5OiBGb3JtQ29udHJvbCxcbiAgICBhZGRyZXNzOiBGb3JtQ29udHJvbCxcbiAgICBsYXQ6IEZvcm1Db250cm9sLFxuICAgIGxvbjogRm9ybUNvbnRyb2xcbiAgfTtcblxuICAvKipcbiAgICogRm9ybSBjb250cm9scyBsb2FkZXIuXG4gICAqIEBwYXJhbSBnZW9ncmFwaGljTG9jYXRpb24gT2JqZWN0IHRoYXQgY29udGFpbnMgaW5mb3JtYXRpb24gYWJvdXQgdGhlIGdlb2dyYXBoaWMgbG9jYXRpb24uXG4gICAqL1xuICBsb2FkQ29udHJvbHMoZ2VvZ3JhcGhpY0xvY2F0aW9uOiBHZW9ncmFwaGljTG9jYXRpb24pIHtcbiAgICB0aGlzLmNvbnRyb2xzLmNpdHkuc2V0VmFsdWUoZ2VvZ3JhcGhpY0xvY2F0aW9uLmNpdHkpO1xuICAgIHRoaXMuY29udHJvbHMuZGVwYXJ0bWVudC5zZXRWYWx1ZShnZW9ncmFwaGljTG9jYXRpb24uZGVwYXJ0bWVudCk7XG4gICAgdGhpcy5jb250cm9scy5yZWdpb24uc2V0VmFsdWUoZ2VvZ3JhcGhpY0xvY2F0aW9uLnJlZ2lvbik7XG4gICAgdGhpcy5jb250cm9scy5jb3VudHJ5LnNldFZhbHVlKGdlb2dyYXBoaWNMb2NhdGlvbi5jb3VudHJ5KTtcbiAgICB0aGlzLmNvbnRyb2xzLmFkZHJlc3Muc2V0VmFsdWUoZ2VvZ3JhcGhpY0xvY2F0aW9uLmFkZHJlc3MpO1xuICAgIGlmIChnZW9ncmFwaGljTG9jYXRpb24uY29vcmRpbmF0ZXMpIHtcbiAgICAgIHRoaXMuY29udHJvbHMubGF0LnNldFZhbHVlKGdlb2dyYXBoaWNMb2NhdGlvbi5jb29yZGluYXRlcy5sYXQpO1xuICAgICAgdGhpcy5jb250cm9scy5sb24uc2V0VmFsdWUoZ2VvZ3JhcGhpY0xvY2F0aW9uLmNvb3JkaW5hdGVzLmxvbik7XG4gICAgfVxuICB9XG5cbiAgLyoqIEZvcm0gY29udHJvbHMgc2F2ZXIgaW50byBhIGdlb2dyYXBoaWMgbG9jYXRpb24uICovXG4gIHNhdmVDb250cm9scygpOiBHZW9ncmFwaGljTG9jYXRpb24ge1xuICAgIHJldHVybiB7XG4gICAgICBjaXR5ICAgICAgIDogdGhpcy5jb250cm9scy5jaXR5LnZhbHVlLFxuICAgICAgZGVwYXJ0bWVudCA6IHRoaXMuY29udHJvbHMuZGVwYXJ0bWVudC52YWx1ZSxcbiAgICAgIHJlZ2lvbiAgICAgOiB0aGlzLmNvbnRyb2xzLnJlZ2lvbi52YWx1ZSxcbiAgICAgIGNvdW50cnkgICAgOiB0aGlzLmNvbnRyb2xzLmNvdW50cnkudmFsdWUsXG4gICAgICBhZGRyZXNzICAgIDogdGhpcy5jb250cm9scy5hZGRyZXNzLnZhbHVlLFxuICAgICAgY29vcmRpbmF0ZXM6IHtcbiAgICAgICAgbGF0OiB0aGlzLmNvbnRyb2xzLmxhdC52YWx1ZSxcbiAgICAgICAgbG9uOiB0aGlzLmNvbnRyb2xzLmxvbi52YWx1ZVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG59XG4iLCJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge2dvb2dsZX0gZnJvbSAnZ29vZ2xlLW1hcHMnO1xuaW1wb3J0IHtCb290c3RhcnRMb2NhdGlvbkZvcm1Hcm91cH0gZnJvbSAnLi4vLi4vbW9kZWxzL2Zvcm0vYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0tZ3JvdXAnO1xuaW1wb3J0IHtCb290c3RhcnRMb2NhdGlvbkZvcm1PcHRpb25zfSBmcm9tICcuLi8uLi9tb2RlbHMvZm9ybS9ib290c3RhcnQtbG9jYXRpb24tZm9ybS1vcHRpb25zJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtbG9jYXRpb24tZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWZvcm0tZmllbGQgW25nU3R5bGVdPVwieyd3aWR0aCcgOiBvcHRpb25zLndpZHRoKyclJ31cIj5cbiAgPG1hdC1sYWJlbD5cbiAgICA8bWF0LWljb24gKm5nSWY9XCJvcHRpb25zLmljb25cIj57e29wdGlvbnMuaWNvbn19PC9tYXQtaWNvbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57eyBvcHRpb25zLmxhYmVsIHwgdHJhbnNsYXRlfX08L3NwYW4+XG4gIDwvbWF0LWxhYmVsPlxuXG4gIDxpbnB1dCBtYXRJbnB1dCBhcHBHb29nbGVQbGFjZXNcbiAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJ0aGlzLmdyb3VwLmNvbnRyb2xzLmFkZHJlc3MudmFsdWVcIlxuICAgICAgICAgKHNlbGVjdGlvbkNoYW5nZSk9XCJ1cGRhdGVQbGFjZSgkZXZlbnQpXCJcbiAgICAgICAgIFtyZXF1aXJlZF09XCJvcHRpb25zLnJlcXVpcmVkXCI+XG48L21hdC1mb3JtLWZpZWxkPlxuYFxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydExvY2F0aW9uRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgLyoqIEFzc29jaWF0ZWQgZm9ybSBncm91cCAqL1xuICBASW5wdXQoKSBncm91cDogQm9vdHN0YXJ0TG9jYXRpb25Gb3JtR3JvdXA7XG5cbiAgLyoqIFNldCBvZiBvcHRpb25zICovXG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydExvY2F0aW9uRm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9zZXREZWZhdWx0T3B0aW9ucygpO1xuICB9XG5cbiAgcHJpdmF0ZSBfc2V0RGVmYXVsdE9wdGlvbnMoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9ucy53aWR0aCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnMud2lkdGggPSAxMDA7XG4gICAgfVxuICB9XG5cbiAgLyoqIFBsYWNlIHVwZGF0ZSAqL1xuICB1cGRhdGVQbGFjZShwbGFjZTogZ29vZ2xlLm1hcHMucGxhY2VzLlBsYWNlUmVzdWx0KSB7XG4gICAgdGhpcy5ncm91cC5jb250cm9scy5jaXR5LnNldFZhbHVlKCcnKTtcbiAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmRlcGFydG1lbnQuc2V0VmFsdWUoJycpO1xuICAgIHRoaXMuZ3JvdXAuY29udHJvbHMucmVnaW9uLnNldFZhbHVlKCcnKTtcbiAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmNvdW50cnkuc2V0VmFsdWUoJycpO1xuXG4gICAgcGxhY2UuYWRkcmVzc19jb21wb25lbnRzLmZvckVhY2goYWRkcmVzc19jb21wb25lbnQgPT4ge1xuICAgICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzLmluZGV4T2YoJ2xvY2FsaXR5JykgPj0gMCkge1xuICAgICAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmNpdHkuc2V0VmFsdWUoYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lKTtcbiAgICAgIH1cbiAgICAgIGlmIChhZGRyZXNzX2NvbXBvbmVudC50eXBlcy5pbmRleE9mKCdhZG1pbmlzdHJhdGl2ZV9hcmVhX2xldmVsXzInKSA+PSAwKSB7XG4gICAgICAgIHRoaXMuZ3JvdXAuY29udHJvbHMuZGVwYXJ0bWVudC5zZXRWYWx1ZShhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWUpO1xuICAgICAgfVxuICAgICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzLmluZGV4T2YoJ2FkbWluaXN0cmF0aXZlX2FyZWFfbGV2ZWxfMScpID49IDApIHtcbiAgICAgICAgdGhpcy5ncm91cC5jb250cm9scy5yZWdpb24uc2V0VmFsdWUoYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lKTtcbiAgICAgIH1cbiAgICAgIGlmIChhZGRyZXNzX2NvbXBvbmVudC50eXBlcy5pbmRleE9mKCdjb3VudHJ5JykgPj0gMCkge1xuICAgICAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmNvdW50cnkuc2V0VmFsdWUoYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHRoaXMuZ3JvdXAuY29udHJvbHMubGF0LnNldFZhbHVlKHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLmxhdCgpKTtcbiAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmxvbi5zZXRWYWx1ZShwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKSk7XG4gICAgdGhpcy5ncm91cC5jb250cm9scy5hZGRyZXNzLnNldFZhbHVlKHBsYWNlLmZvcm1hdHRlZF9hZGRyZXNzKTtcbiAgfVxuXG59XG4iLCJpbXBvcnQge0RpcmVjdGl2ZSwgRWxlbWVudFJlZiwgRXZlbnRFbWl0dGVyLCBPbkluaXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBnb29nbGUgfSBmcm9tICdnb29nbGUtbWFwcyc7XG5cbkBEaXJlY3RpdmUoe1xuICAgICAgICAgICAgIHNlbGVjdG9yOiAnW2FwcEdvb2dsZVBsYWNlc10nXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgR29vZ2xlUGxhY2VzRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0IHtcbiAgQE91dHB1dCgpIHNlbGVjdGlvbkNoYW5nZTogRXZlbnRFbWl0dGVyPGdvb2dsZS5tYXBzLnBsYWNlcy5QbGFjZVJlc3VsdD4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIHByaXZhdGUgZWxlbWVudDogSFRNTElucHV0RWxlbWVudDtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsUmVmOiBFbGVtZW50UmVmKSB7XG4gICAgdGhpcy5lbGVtZW50ID0gZWxSZWYubmF0aXZlRWxlbWVudDtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGNvbnN0IGF1dG9jb21wbGV0ZSA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuQXV0b2NvbXBsZXRlKHRoaXMuZWxlbWVudCk7XG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIoYXV0b2NvbXBsZXRlLCAncGxhY2VfY2hhbmdlZCcsICgpID0+IHtcbiAgICAgIHRoaXMuc2VsZWN0aW9uQ2hhbmdlLmVtaXQoYXV0b2NvbXBsZXRlLmdldFBsYWNlKCkpO1xuICAgIH0pO1xuICB9XG5cbn1cbiIsImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCb290c3RhcnRMb2NhdGlvbkZvcm1Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtbG9jYXRpb24tZm9ybS9ib290c3RhcnQtbG9jYXRpb24tZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtNYXRGb3JtRmllbGRNb2R1bGUsIE1hdEljb25Nb2R1bGUsIE1hdElucHV0TW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7Rm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7VHJhbnNsYXRlTW9kdWxlfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7R29vZ2xlUGxhY2VzRGlyZWN0aXZlfSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0vZ29vZ2xlLXBsYWNlcy9nb29nbGUtcGxhY2VzLmRpcmVjdGl2ZSc7XG5cbkBOZ01vZHVsZSh7XG4gICAgICAgICAgICBpbXBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgICAgICAgICBGb3Jtc01vZHVsZSxcbiAgICAgICAgICAgICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICAgICAgICAgICAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0SWNvbk1vZHVsZSxcbiAgICAgICAgICAgICAgVHJhbnNsYXRlTW9kdWxlXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgICAgICAgIEJvb3RzdGFydExvY2F0aW9uRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgR29vZ2xlUGxhY2VzRGlyZWN0aXZlXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZXhwb3J0cyAgICAgOiBbXG4gICAgICAgICAgICAgIEJvb3RzdGFydExvY2F0aW9uRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgR29vZ2xlUGxhY2VzRGlyZWN0aXZlXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgcHJvdmlkZXJzICAgOiBbXSxcbiAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEdlb2dyYXBoeU1vZHVsZSB7XG59XG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLGdDQVV3QyxTQUFRLFNBQVM7Ozs7O0lBRXZELFlBQVksZUFBNEUsRUFDNUUsY0FBNkQ7O1FBQ3ZFLE1BQU0sT0FBTyxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7O1FBQ2xDLE1BQU0sUUFBUSxHQUFHO1lBQ2YsSUFBSSxFQUFRLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO1lBQy9CLFVBQVUsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztZQUMvQixNQUFNLEVBQU0sT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7WUFDL0IsT0FBTyxFQUFLLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO1lBQy9CLE9BQU8sRUFBSyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztZQUMvQixHQUFHLEVBQVMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDOUIsR0FBRyxFQUFTLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1NBQy9CLENBQUM7UUFDRixLQUFLLENBQUMsUUFBUSxFQUFFLGVBQWUsRUFBRSxjQUFjLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztLQUMxQjs7Ozs7O0lBaUJELFlBQVksQ0FBQyxrQkFBc0M7UUFDakQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNqRSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMzRCxJQUFJLGtCQUFrQixDQUFDLFdBQVcsRUFBRTtZQUNsQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9ELElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDaEU7S0FDRjs7Ozs7SUFHRCxZQUFZO1FBQ1YsT0FBTztZQUNMLElBQUksRUFBUyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLO1lBQ3JDLFVBQVUsRUFBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLO1lBQzNDLE1BQU0sRUFBTyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLO1lBQ3ZDLE9BQU8sRUFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQ3hDLE9BQU8sRUFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO1lBQ3hDLFdBQVcsRUFBRTtnQkFDWCxHQUFHLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSztnQkFDNUIsR0FBRyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEtBQUs7YUFDN0I7U0FDRixDQUFBO0tBQ0Y7Q0FFRjs7Ozs7O0FDdEVEO0lBOEJFO0tBQ0M7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7S0FDM0I7Ozs7SUFFTyxrQkFBa0I7UUFDeEIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssS0FBSyxTQUFTLEVBQUU7WUFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1NBQzFCOzs7Ozs7O0lBSUgsV0FBVyxDQUFDLEtBQXFDO1FBQy9DLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFFekMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxpQkFBaUI7WUFDaEQsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDcEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNoRTtZQUNELElBQUksaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDdkUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUN0RTtZQUNELElBQUksaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDdkUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNsRTtZQUNELElBQUksaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ25ELElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDbkU7U0FDRixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7S0FDL0Q7OztZQS9ERixTQUFTLFNBQUM7Z0JBQ0UsUUFBUSxFQUFLLHlCQUF5QjtnQkFDdEMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7O0NBYXRCO2FBQ1c7Ozs7O29CQUlULEtBQUs7c0JBR0wsS0FBSzs7Ozs7OztBQzVCUjs7OztJQVVFLFlBQW9CLEtBQWlCO1FBQWpCLFVBQUssR0FBTCxLQUFLLENBQVk7K0JBSHFDLElBQUksWUFBWSxFQUFFO1FBSTFGLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLGFBQWEsQ0FBQztLQUNwQzs7OztJQUVELFFBQVE7O1FBQ04sTUFBTSxZQUFZLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3ZFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsZUFBZSxFQUFFO1lBQzNELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1NBQ3BELENBQUMsQ0FBQztLQUNKOzs7WUFoQkYsU0FBUyxTQUFDO2dCQUNFLFFBQVEsRUFBRSxtQkFBbUI7YUFDOUI7Ozs7WUFMTyxVQUFVOzs7OEJBTzFCLE1BQU07Ozs7Ozs7QUNQVDs7O1lBUUMsUUFBUSxTQUFDO2dCQUNFLE9BQU8sRUFBTztvQkFDWixZQUFZO29CQUNaLFdBQVc7b0JBQ1gsbUJBQW1CO29CQUNuQixrQkFBa0I7b0JBQ2xCLGNBQWM7b0JBQ2QsYUFBYTtvQkFDYixlQUFlO2lCQUNoQjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1osOEJBQThCO29CQUM5QixxQkFBcUI7aUJBQ3RCO2dCQUNELE9BQU8sRUFBTztvQkFDWiw4QkFBOEI7b0JBQzlCLHFCQUFxQjtpQkFDdEI7Z0JBQ0QsU0FBUyxFQUFLLEVBQUU7YUFDakI7Ozs7Ozs7Ozs7Ozs7OzsifQ==