/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function GeographicLocationCoordinates() { }
/**
 * Latitude
 * @type {?}
 */
GeographicLocationCoordinates.prototype.lat;
/**
 * Longitude
 * @type {?}
 */
GeographicLocationCoordinates.prototype.lon;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2VvZ3JhcGhpYy1sb2NhdGlvbi1jb29yZGluYXRlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1nZW9ncmFwaHkvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2xvY2F0aW9uL2dlb2dyYXBoaWMtbG9jYXRpb24tY29vcmRpbmF0ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgR2VvZ3JhcGhpY0xvY2F0aW9uQ29vcmRpbmF0ZXMge1xuICAvKiogTGF0aXR1ZGUgKi9cbiAgbGF0OiBudW1iZXI7XG4gIC8qKiBMb25naXR1ZGUgKi9cbiAgbG9uOiBudW1iZXI7XG59XG4iXX0=