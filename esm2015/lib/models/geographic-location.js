/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function GeographicLocation() { }
/**
 * City
 * @type {?}
 */
GeographicLocation.prototype.city;
/**
 * Department (administrative area level 1)
 * @type {?}
 */
GeographicLocation.prototype.department;
/**
 * Region (administrative area level 2)
 * @type {?}
 */
GeographicLocation.prototype.region;
/**
 * Country
 * @type {?}
 */
GeographicLocation.prototype.country;
/**
 * Full address
 * @type {?}
 */
GeographicLocation.prototype.address;
/**
 * Geographic coordinates
 * @type {?}
 */
GeographicLocation.prototype.coordinates;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2VvZ3JhcGhpYy1sb2NhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1nZW9ncmFwaHkvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2dlb2dyYXBoaWMtbG9jYXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7R2VvZ3JhcGhpY0xvY2F0aW9uQ29vcmRpbmF0ZXN9IGZyb20gJy4vZ2VvZ3JhcGhpYy1sb2NhdGlvbi1jb29yZGluYXRlcyc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgR2VvZ3JhcGhpY0xvY2F0aW9uIHtcbiAgLyoqIENpdHkgKi9cbiAgY2l0eTogc3RyaW5nO1xuICAvKiogRGVwYXJ0bWVudCAoYWRtaW5pc3RyYXRpdmUgYXJlYSBsZXZlbCAxKSAqL1xuICBkZXBhcnRtZW50OiBzdHJpbmc7XG4gIC8qKiBSZWdpb24gKGFkbWluaXN0cmF0aXZlIGFyZWEgbGV2ZWwgMikgKi9cbiAgcmVnaW9uOiBzdHJpbmc7XG4gIC8qKiBDb3VudHJ5ICovXG4gIGNvdW50cnk6IHN0cmluZztcbiAgLyoqIEZ1bGwgYWRkcmVzcyAqL1xuICBhZGRyZXNzOiBzdHJpbmc7XG4gIC8qKiBHZW9ncmFwaGljIGNvb3JkaW5hdGVzICovXG4gIGNvb3JkaW5hdGVzOiBHZW9ncmFwaGljTG9jYXRpb25Db29yZGluYXRlcztcbn1cbiJdfQ==