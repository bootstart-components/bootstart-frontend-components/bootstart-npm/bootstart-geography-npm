/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { BootstartLocationFormComponent } from './components/bootstart-location-form/bootstart-location-form.component';
import { MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { GooglePlacesDirective } from './components/bootstart-location-form/google-places/google-places.directive';
export class BootstartGeographyModule {
}
BootstartGeographyModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MatFormFieldModule,
                    MatInputModule,
                    MatIconModule,
                    TranslateModule
                ],
                declarations: [
                    BootstartLocationFormComponent,
                    GooglePlacesDirective
                ],
                exports: [
                    BootstartLocationFormComponent,
                    GooglePlacesDirective
                ],
                providers: [],
            },] },
];

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWdlb2dyYXBoeS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZ2VvZ3JhcGh5LyIsInNvdXJjZXMiOlsibGliL2Jvb3RzdGFydC1nZW9ncmFwaHkubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyw4QkFBOEIsRUFBQyxNQUFNLHdFQUF3RSxDQUFDO0FBQ3RILE9BQU8sRUFBQyxrQkFBa0IsRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFDcEYsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxXQUFXLEVBQUUsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNoRSxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFDcEQsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0sNEVBQTRFLENBQUM7QUFzQmpILE1BQU07OztZQXBCTCxRQUFRLFNBQUM7Z0JBQ0UsT0FBTyxFQUFPO29CQUNaLFlBQVk7b0JBQ1osV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGtCQUFrQjtvQkFDbEIsY0FBYztvQkFDZCxhQUFhO29CQUNiLGVBQWU7aUJBQ2hCO2dCQUNELFlBQVksRUFBRTtvQkFDWiw4QkFBOEI7b0JBQzlCLHFCQUFxQjtpQkFDdEI7Z0JBQ0QsT0FBTyxFQUFPO29CQUNaLDhCQUE4QjtvQkFDOUIscUJBQXFCO2lCQUN0QjtnQkFDRCxTQUFTLEVBQUssRUFBRTthQUNqQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7TmdNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtCb290c3RhcnRMb2NhdGlvbkZvcm1Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtbG9jYXRpb24tZm9ybS9ib290c3RhcnQtbG9jYXRpb24tZm9ybS5jb21wb25lbnQnO1xuaW1wb3J0IHtNYXRGb3JtRmllbGRNb2R1bGUsIE1hdEljb25Nb2R1bGUsIE1hdElucHV0TW9kdWxlfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQge0NvbW1vbk1vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7Rm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7VHJhbnNsYXRlTW9kdWxlfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7R29vZ2xlUGxhY2VzRGlyZWN0aXZlfSBmcm9tICcuL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0vZ29vZ2xlLXBsYWNlcy9nb29nbGUtcGxhY2VzLmRpcmVjdGl2ZSc7XG5cbkBOZ01vZHVsZSh7XG4gICAgICAgICAgICBpbXBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgICAgICAgICBGb3Jtc01vZHVsZSxcbiAgICAgICAgICAgICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICAgICAgICAgICAgTWF0Rm9ybUZpZWxkTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRJbnB1dE1vZHVsZSxcbiAgICAgICAgICAgICAgTWF0SWNvbk1vZHVsZSxcbiAgICAgICAgICAgICAgVHJhbnNsYXRlTW9kdWxlXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgICAgICAgIEJvb3RzdGFydExvY2F0aW9uRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgR29vZ2xlUGxhY2VzRGlyZWN0aXZlXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgZXhwb3J0cyAgICAgOiBbXG4gICAgICAgICAgICAgIEJvb3RzdGFydExvY2F0aW9uRm9ybUNvbXBvbmVudCxcbiAgICAgICAgICAgICAgR29vZ2xlUGxhY2VzRGlyZWN0aXZlXG4gICAgICAgICAgICBdLFxuICAgICAgICAgICAgcHJvdmlkZXJzICAgOiBbXSxcbiAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEdlb2dyYXBoeU1vZHVsZSB7XG59XG4iXX0=