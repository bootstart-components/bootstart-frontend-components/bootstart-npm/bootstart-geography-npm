/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { BootstartGeographyFormGroup } from '../../models/bootstart-geography-form-group';
export class BootstartGeographyQuestionComponent {
    constructor() {
        /**
         * Is the question required?
         */
        this.required = false;
        /**
         * Is the question displayed with a width of 100%? True by default.
         */
        this.fullWidth = false;
    }
    /**
     * @param {?} place
     * @return {?}
     */
    updatePlace(place) {
        this.formGroup.controls.city.setValue('');
        this.formGroup.controls.department.setValue('');
        this.formGroup.controls.region.setValue('');
        this.formGroup.controls.country.setValue('');
        place.address_components.forEach(address_component => {
            if (address_component.types.indexOf('locality') >= 0) {
                this.formGroup.controls.city.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('administrative_area_level_2') >= 0) {
                this.formGroup.controls.department.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('administrative_area_level_1') >= 0) {
                this.formGroup.controls.region.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('country') >= 0) {
                this.formGroup.controls.country.setValue(address_component.long_name);
            }
        });
        this.formGroup.controls.lat.setValue(place.geometry.location.lat());
        this.formGroup.controls.lon.setValue(place.geometry.location.lng());
        this.formGroup.controls.address.setValue(place.formatted_address);
    }
    /**
     * @return {?}
     */
    getPlaceholder() {
        return ((this.formGroup.controls.address.value === null || this.formGroup.controls.address.value === '') ?
            this.questionPlaceholder : this.formGroup.controls.address.value);
    }
}
BootstartGeographyQuestionComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-geography-question',
                template: `<mat-form-field [ngClass]="{'full-width': fullWidth}" floatLabel="never">
  <mat-label>
    <mat-icon *ngIf="questionIcon">{{questionIcon}}</mat-icon>
    <span *ngIf="questionIcon">&nbsp;</span>
    <span *ngIf="questionLabel">{{ questionLabel | translate}}</span>
  </mat-label>

  <input matInput appGooglePlaces
         type="text"
         [placeholder]="getPlaceholder()"
         (selectionChange)="updatePlace($event)"
         [required]="required">
</mat-form-field>
`
            },] },
];
/** @nocollapse */
BootstartGeographyQuestionComponent.ctorParameters = () => [];
BootstartGeographyQuestionComponent.propDecorators = {
    formGroup: [{ type: Input }],
    questionLabel: [{ type: Input }],
    questionIcon: [{ type: Input }],
    questionPlaceholder: [{ type: Input }],
    required: [{ type: Input }],
    fullWidth: [{ type: Input }]
};
if (false) {
    /**
     * Form group
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.formGroup;
    /**
     * Optional question label. Translation is applied.
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.questionLabel;
    /**
     * Optional question icon (Angular Material icon)
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.questionIcon;
    /**
     * Optional placeholder
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.questionPlaceholder;
    /**
     * Is the question required?
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.required;
    /**
     * Is the question displayed with a width of 100%? True by default.
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.fullWidth;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWdlb2dyYXBoeS1xdWVzdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZ2VvZ3JhcGh5LyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWdlb2dyYXBoeS1xdWVzdGlvbi9ib290c3RhcnQtZ2VvZ3JhcGh5LXF1ZXN0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFFL0MsT0FBTyxFQUFDLDJCQUEyQixFQUFDLE1BQU0sNkNBQTZDLENBQUM7QUFtQnhGLE1BQU07SUFjSjs7Ozt3QkFKb0IsS0FBSzs7Ozt5QkFFSixLQUFLO0tBR3pCOzs7OztJQUdELFdBQVcsQ0FBQyxLQUFxQztRQUMvQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRTdDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsRUFBRTtZQUNuRCxFQUFFLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDcEU7WUFDRCxFQUFFLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLDZCQUE2QixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDeEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUMxRTtZQUNELEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4RSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3RFO1lBQ0QsRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3ZFO1NBQ0YsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUNwRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0tBQ25FOzs7O0lBRUQsY0FBYztRQUNaLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2xHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0tBQzNFOzs7WUFoRUYsU0FBUyxTQUFDO2dCQUNFLFFBQVEsRUFBSyw4QkFBOEI7Z0JBQzNDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7OztDQWF0QjthQUNXOzs7Ozt3QkFHVCxLQUFLOzRCQUVMLEtBQUs7MkJBRUwsS0FBSztrQ0FFTCxLQUFLO3VCQUVMLEtBQUs7d0JBRUwsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge2dvb2dsZX0gZnJvbSAnZ29vZ2xlLW1hcHMnO1xuaW1wb3J0IHtCb290c3RhcnRHZW9ncmFwaHlGb3JtR3JvdXB9IGZyb20gJy4uLy4uL21vZGVscy9ib290c3RhcnQtZ2VvZ3JhcGh5LWZvcm0tZ3JvdXAnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1nZW9ncmFwaHktcXVlc3Rpb24nLFxuICAgICAgICAgICAgIHRlbXBsYXRlOiBgPG1hdC1mb3JtLWZpZWxkIFtuZ0NsYXNzXT1cInsnZnVsbC13aWR0aCc6IGZ1bGxXaWR0aH1cIiBmbG9hdExhYmVsPVwibmV2ZXJcIj5cbiAgPG1hdC1sYWJlbD5cbiAgICA8bWF0LWljb24gKm5nSWY9XCJxdWVzdGlvbkljb25cIj57e3F1ZXN0aW9uSWNvbn19PC9tYXQtaWNvbj5cbiAgICA8c3BhbiAqbmdJZj1cInF1ZXN0aW9uSWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgICA8c3BhbiAqbmdJZj1cInF1ZXN0aW9uTGFiZWxcIj57eyBxdWVzdGlvbkxhYmVsIHwgdHJhbnNsYXRlfX08L3NwYW4+XG4gIDwvbWF0LWxhYmVsPlxuXG4gIDxpbnB1dCBtYXRJbnB1dCBhcHBHb29nbGVQbGFjZXNcbiAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJnZXRQbGFjZWhvbGRlcigpXCJcbiAgICAgICAgIChzZWxlY3Rpb25DaGFuZ2UpPVwidXBkYXRlUGxhY2UoJGV2ZW50KVwiXG4gICAgICAgICBbcmVxdWlyZWRdPVwicmVxdWlyZWRcIj5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0R2VvZ3JhcGh5UXVlc3Rpb25Db21wb25lbnQge1xuICAvKiogRm9ybSBncm91cCAqL1xuICBASW5wdXQoKSBmb3JtR3JvdXA6IEJvb3RzdGFydEdlb2dyYXBoeUZvcm1Hcm91cDtcbiAgLyoqIE9wdGlvbmFsIHF1ZXN0aW9uIGxhYmVsLiBUcmFuc2xhdGlvbiBpcyBhcHBsaWVkLiAqL1xuICBASW5wdXQoKSBxdWVzdGlvbkxhYmVsOiBzdHJpbmc7XG4gIC8qKiBPcHRpb25hbCBxdWVzdGlvbiBpY29uIChBbmd1bGFyIE1hdGVyaWFsIGljb24pICovXG4gIEBJbnB1dCgpIHF1ZXN0aW9uSWNvbjogc3RyaW5nO1xuICAvKiogT3B0aW9uYWwgcGxhY2Vob2xkZXIgKi9cbiAgQElucHV0KCkgcXVlc3Rpb25QbGFjZWhvbGRlcjogc3RyaW5nO1xuICAvKiogSXMgdGhlIHF1ZXN0aW9uIHJlcXVpcmVkPyAqL1xuICBASW5wdXQoKSByZXF1aXJlZCA9IGZhbHNlO1xuICAvKiogSXMgdGhlIHF1ZXN0aW9uIGRpc3BsYXllZCB3aXRoIGEgd2lkdGggb2YgMTAwJT8gVHJ1ZSBieSBkZWZhdWx0LiAqL1xuICBASW5wdXQoKSBmdWxsV2lkdGggPSBmYWxzZTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG5cbiAgdXBkYXRlUGxhY2UocGxhY2U6IGdvb2dsZS5tYXBzLnBsYWNlcy5QbGFjZVJlc3VsdCkge1xuICAgIHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLmNpdHkuc2V0VmFsdWUoJycpO1xuICAgIHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLmRlcGFydG1lbnQuc2V0VmFsdWUoJycpO1xuICAgIHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLnJlZ2lvbi5zZXRWYWx1ZSgnJyk7XG4gICAgdGhpcy5mb3JtR3JvdXAuY29udHJvbHMuY291bnRyeS5zZXRWYWx1ZSgnJyk7XG5cbiAgICBwbGFjZS5hZGRyZXNzX2NvbXBvbmVudHMuZm9yRWFjaChhZGRyZXNzX2NvbXBvbmVudCA9PiB7XG4gICAgICBpZiAoYWRkcmVzc19jb21wb25lbnQudHlwZXMuaW5kZXhPZignbG9jYWxpdHknKSA+PSAwKSB7XG4gICAgICAgIHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLmNpdHkuc2V0VmFsdWUoYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lKTtcbiAgICAgIH1cbiAgICAgIGlmIChhZGRyZXNzX2NvbXBvbmVudC50eXBlcy5pbmRleE9mKCdhZG1pbmlzdHJhdGl2ZV9hcmVhX2xldmVsXzInKSA+PSAwKSB7XG4gICAgICAgIHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLmRlcGFydG1lbnQuc2V0VmFsdWUoYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lKTtcbiAgICAgIH1cbiAgICAgIGlmIChhZGRyZXNzX2NvbXBvbmVudC50eXBlcy5pbmRleE9mKCdhZG1pbmlzdHJhdGl2ZV9hcmVhX2xldmVsXzEnKSA+PSAwKSB7XG4gICAgICAgIHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLnJlZ2lvbi5zZXRWYWx1ZShhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWUpO1xuICAgICAgfVxuICAgICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzLmluZGV4T2YoJ2NvdW50cnknKSA+PSAwKSB7XG4gICAgICAgIHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLmNvdW50cnkuc2V0VmFsdWUoYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLmxhdC5zZXRWYWx1ZShwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbi5sYXQoKSk7XG4gICAgdGhpcy5mb3JtR3JvdXAuY29udHJvbHMubG9uLnNldFZhbHVlKHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLmxuZygpKTtcbiAgICB0aGlzLmZvcm1Hcm91cC5jb250cm9scy5hZGRyZXNzLnNldFZhbHVlKHBsYWNlLmZvcm1hdHRlZF9hZGRyZXNzKTtcbiAgfVxuXG4gIGdldFBsYWNlaG9sZGVyKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuICgodGhpcy5mb3JtR3JvdXAuY29udHJvbHMuYWRkcmVzcy52YWx1ZSA9PT0gbnVsbCB8fCB0aGlzLmZvcm1Hcm91cC5jb250cm9scy5hZGRyZXNzLnZhbHVlID09PSAnJykgP1xuICAgICAgICAgICAgdGhpcy5xdWVzdGlvblBsYWNlaG9sZGVyIDogdGhpcy5mb3JtR3JvdXAuY29udHJvbHMuYWRkcmVzcy52YWx1ZSk7XG4gIH1cbn1cbiJdfQ==