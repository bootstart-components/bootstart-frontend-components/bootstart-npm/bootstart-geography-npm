/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Directive, ElementRef, EventEmitter, Output } from '@angular/core';
export class GooglePlacesDirective {
    /**
     * @param {?} elRef
     */
    constructor(elRef) {
        this.elRef = elRef;
        this.selectionChange = new EventEmitter();
        this.element = elRef.nativeElement;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const autocomplete = new google.maps.places.Autocomplete(this.element);
        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            this.selectionChange.emit(autocomplete.getPlace());
        });
    }
}
GooglePlacesDirective.decorators = [
    { type: Directive, args: [{
                selector: '[appGooglePlaces]'
            },] },
];
/** @nocollapse */
GooglePlacesDirective.ctorParameters = () => [
    { type: ElementRef }
];
GooglePlacesDirective.propDecorators = {
    selectionChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    GooglePlacesDirective.prototype.selectionChange;
    /** @type {?} */
    GooglePlacesDirective.prototype.element;
    /** @type {?} */
    GooglePlacesDirective.prototype.elRef;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlLXBsYWNlcy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZ2VvZ3JhcGh5LyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWdlb2dyYXBoeS1xdWVzdGlvbi9nb29nbGUtcGxhY2VzL2dvb2dsZS1wbGFjZXMuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQVUsTUFBTSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBTWxGLE1BQU07Ozs7SUFJSixZQUFvQixLQUFpQjtRQUFqQixVQUFLLEdBQUwsS0FBSyxDQUFZOytCQUhxQyxJQUFJLFlBQVksRUFBRTtRQUkxRixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUM7S0FDcEM7Ozs7SUFFRCxRQUFROztRQUNOLE1BQU0sWUFBWSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN2RSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLGVBQWUsRUFBRSxHQUFHLEVBQUU7WUFDaEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7U0FDcEQsQ0FBQyxDQUFDO0tBQ0o7OztZQWhCRixTQUFTLFNBQUM7Z0JBQ0UsUUFBUSxFQUFFLG1CQUFtQjthQUM5Qjs7OztZQUxPLFVBQVU7Ozs4QkFPMUIsTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7RGlyZWN0aXZlLCBFbGVtZW50UmVmLCBFdmVudEVtaXR0ZXIsIE9uSW5pdCwgT3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IGdvb2dsZSB9IGZyb20gJ2dvb2dsZS1tYXBzJztcblxuQERpcmVjdGl2ZSh7XG4gICAgICAgICAgICAgc2VsZWN0b3I6ICdbYXBwR29vZ2xlUGxhY2VzXSdcbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBHb29nbGVQbGFjZXNEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQge1xuICBAT3V0cHV0KCkgc2VsZWN0aW9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8Z29vZ2xlLm1hcHMucGxhY2VzLlBsYWNlUmVzdWx0PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgcHJpdmF0ZSBlbGVtZW50OiBIVE1MSW5wdXRFbGVtZW50O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZWxSZWY6IEVsZW1lbnRSZWYpIHtcbiAgICB0aGlzLmVsZW1lbnQgPSBlbFJlZi5uYXRpdmVFbGVtZW50O1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgY29uc3QgYXV0b2NvbXBsZXRlID0gbmV3IGdvb2dsZS5tYXBzLnBsYWNlcy5BdXRvY29tcGxldGUodGhpcy5lbGVtZW50KTtcbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcihhdXRvY29tcGxldGUsICdwbGFjZV9jaGFuZ2VkJywgKCkgPT4ge1xuICAgICAgdGhpcy5zZWxlY3Rpb25DaGFuZ2UuZW1pdChhdXRvY29tcGxldGUuZ2V0UGxhY2UoKSk7XG4gICAgfSk7XG4gIH1cblxufVxuIl19