/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Directive, ElementRef, EventEmitter, Output } from '@angular/core';
export class GooglePlacesDirective {
    /**
     * @param {?} elRef
     */
    constructor(elRef) {
        this.elRef = elRef;
        this.selectionChange = new EventEmitter();
        this.element = elRef.nativeElement;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const autocomplete = new google.maps.places.Autocomplete(this.element);
        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            this.selectionChange.emit(autocomplete.getPlace());
        });
    }
}
GooglePlacesDirective.decorators = [
    { type: Directive, args: [{
                selector: '[appGooglePlaces]'
            },] },
];
/** @nocollapse */
GooglePlacesDirective.ctorParameters = () => [
    { type: ElementRef }
];
GooglePlacesDirective.propDecorators = {
    selectionChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    GooglePlacesDirective.prototype.selectionChange;
    /** @type {?} */
    GooglePlacesDirective.prototype.element;
    /** @type {?} */
    GooglePlacesDirective.prototype.elRef;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlLXBsYWNlcy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZ2VvZ3JhcGh5LyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0vZ29vZ2xlLXBsYWNlcy9nb29nbGUtcGxhY2VzLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFVLE1BQU0sRUFBQyxNQUFNLGVBQWUsQ0FBQztBQU1sRixNQUFNOzs7O0lBSUosWUFBb0IsS0FBaUI7UUFBakIsVUFBSyxHQUFMLEtBQUssQ0FBWTsrQkFIcUMsSUFBSSxZQUFZLEVBQUU7UUFJMUYsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsYUFBYSxDQUFDO0tBQ3BDOzs7O0lBRUQsUUFBUTs7UUFDTixNQUFNLFlBQVksR0FBRyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkUsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxlQUFlLEVBQUUsR0FBRyxFQUFFO1lBQ2hFLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1NBQ3BELENBQUMsQ0FBQztLQUNKOzs7WUFoQkYsU0FBUyxTQUFDO2dCQUNFLFFBQVEsRUFBRSxtQkFBbUI7YUFDOUI7Ozs7WUFMTyxVQUFVOzs7OEJBTzFCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0RpcmVjdGl2ZSwgRWxlbWVudFJlZiwgRXZlbnRFbWl0dGVyLCBPbkluaXQsIE91dHB1dH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBnb29nbGUgfSBmcm9tICdnb29nbGUtbWFwcyc7XG5cbkBEaXJlY3RpdmUoe1xuICAgICAgICAgICAgIHNlbGVjdG9yOiAnW2FwcEdvb2dsZVBsYWNlc10nXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgR29vZ2xlUGxhY2VzRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0IHtcbiAgQE91dHB1dCgpIHNlbGVjdGlvbkNoYW5nZTogRXZlbnRFbWl0dGVyPGdvb2dsZS5tYXBzLnBsYWNlcy5QbGFjZVJlc3VsdD4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gIHByaXZhdGUgZWxlbWVudDogSFRNTElucHV0RWxlbWVudDtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsUmVmOiBFbGVtZW50UmVmKSB7XG4gICAgdGhpcy5lbGVtZW50ID0gZWxSZWYubmF0aXZlRWxlbWVudDtcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGNvbnN0IGF1dG9jb21wbGV0ZSA9IG5ldyBnb29nbGUubWFwcy5wbGFjZXMuQXV0b2NvbXBsZXRlKHRoaXMuZWxlbWVudCk7XG4gICAgZ29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIoYXV0b2NvbXBsZXRlLCAncGxhY2VfY2hhbmdlZCcsICgpID0+IHtcbiAgICAgIHRoaXMuc2VsZWN0aW9uQ2hhbmdlLmVtaXQoYXV0b2NvbXBsZXRlLmdldFBsYWNlKCkpO1xuICAgIH0pO1xuICB9XG5cbn1cbiJdfQ==