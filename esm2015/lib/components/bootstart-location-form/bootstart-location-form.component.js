/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { BootstartLocationFormGroup } from '../../models/form/bootstart-location-form-group';
export class BootstartLocationFormComponent {
    constructor() {
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._setDefaultOptions();
    }
    /**
     * @return {?}
     */
    _setDefaultOptions() {
        if (this.options.width === undefined) {
            this.options.width = 100;
        }
    }
    /**
     * Place update
     * @param {?} place
     * @return {?}
     */
    updatePlace(place) {
        this.group.controls.city.setValue('');
        this.group.controls.department.setValue('');
        this.group.controls.region.setValue('');
        this.group.controls.country.setValue('');
        place.address_components.forEach(address_component => {
            if (address_component.types.indexOf('locality') >= 0) {
                this.group.controls.city.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('administrative_area_level_2') >= 0) {
                this.group.controls.department.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('administrative_area_level_1') >= 0) {
                this.group.controls.region.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('country') >= 0) {
                this.group.controls.country.setValue(address_component.long_name);
            }
        });
        this.group.controls.lat.setValue(place.geometry.location.lat());
        this.group.controls.lon.setValue(place.geometry.location.lng());
        this.group.controls.address.setValue(place.formatted_address);
    }
}
BootstartLocationFormComponent.decorators = [
    { type: Component, args: [{
                selector: 'bootstart-location-form',
                template: `<mat-form-field [ngStyle]="{'width' : options.width+'%'}">
  <mat-label>
    <mat-icon *ngIf="options.icon">{{options.icon}}</mat-icon>
    <span *ngIf="options.icon">&nbsp;</span>
    <span *ngIf="options.label">{{ options.label | translate}}</span>
  </mat-label>

  <input matInput appGooglePlaces
         type="text"
         [placeholder]="this.group.controls.address.value"
         (selectionChange)="updatePlace($event)"
         [required]="options.required">
</mat-form-field>
`
            },] },
];
/** @nocollapse */
BootstartLocationFormComponent.ctorParameters = () => [];
BootstartLocationFormComponent.propDecorators = {
    group: [{ type: Input }],
    options: [{ type: Input }]
};
if (false) {
    /**
     * Associated form group
     * @type {?}
     */
    BootstartLocationFormComponent.prototype.group;
    /**
     * Set of options
     * @type {?}
     */
    BootstartLocationFormComponent.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWdlb2dyYXBoeS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFFdkQsT0FBTyxFQUFDLDBCQUEwQixFQUFDLE1BQU0saURBQWlELENBQUM7QUFvQjNGLE1BQU07SUFRSjtLQUNDOzs7O0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0tBQzNCOzs7O0lBRU8sa0JBQWtCO1FBQ3hCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1NBQzFCOzs7Ozs7O0lBSUgsV0FBVyxDQUFDLEtBQXFDO1FBQy9DLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFFekMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO1lBQ25ELEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckQsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNoRTtZQUNELEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4RSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3RFO1lBQ0QsRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDbEU7WUFDRCxFQUFFLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDbkU7U0FDRixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7S0FDL0Q7OztZQS9ERixTQUFTLFNBQUM7Z0JBQ0UsUUFBUSxFQUFLLHlCQUF5QjtnQkFDdEMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7O0NBYXRCO2FBQ1c7Ozs7O29CQUlULEtBQUs7c0JBR0wsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Z29vZ2xlfSBmcm9tICdnb29nbGUtbWFwcyc7XG5pbXBvcnQge0Jvb3RzdGFydExvY2F0aW9uRm9ybUdyb3VwfSBmcm9tICcuLi8uLi9tb2RlbHMvZm9ybS9ib290c3RhcnQtbG9jYXRpb24tZm9ybS1ncm91cCc7XG5pbXBvcnQge0Jvb3RzdGFydExvY2F0aW9uRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9mb3JtL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtLW9wdGlvbnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtZm9ybS1maWVsZCBbbmdTdHlsZV09XCJ7J3dpZHRoJyA6IG9wdGlvbnMud2lkdGgrJyUnfVwiPlxuICA8bWF0LWxhYmVsPlxuICAgIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7IG9wdGlvbnMubGFiZWwgfCB0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgPC9tYXQtbGFiZWw+XG5cbiAgPGlucHV0IG1hdElucHV0IGFwcEdvb2dsZVBsYWNlc1xuICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgW3BsYWNlaG9sZGVyXT1cInRoaXMuZ3JvdXAuY29udHJvbHMuYWRkcmVzcy52YWx1ZVwiXG4gICAgICAgICAoc2VsZWN0aW9uQ2hhbmdlKT1cInVwZGF0ZVBsYWNlKCRldmVudClcIlxuICAgICAgICAgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIj5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0TG9jYXRpb25Gb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAvKiogQXNzb2NpYXRlZCBmb3JtIGdyb3VwICovXG4gIEBJbnB1dCgpIGdyb3VwOiBCb290c3RhcnRMb2NhdGlvbkZvcm1Hcm91cDtcblxuICAvKiogU2V0IG9mIG9wdGlvbnMgKi9cbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0TG9jYXRpb25Gb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3NldERlZmF1bHRPcHRpb25zKCk7XG4gIH1cblxuICBwcml2YXRlIF9zZXREZWZhdWx0T3B0aW9ucygpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zLndpZHRoID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9ucy53aWR0aCA9IDEwMDtcbiAgICB9XG4gIH1cblxuICAvKiogUGxhY2UgdXBkYXRlICovXG4gIHVwZGF0ZVBsYWNlKHBsYWNlOiBnb29nbGUubWFwcy5wbGFjZXMuUGxhY2VSZXN1bHQpIHtcbiAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmNpdHkuc2V0VmFsdWUoJycpO1xuICAgIHRoaXMuZ3JvdXAuY29udHJvbHMuZGVwYXJ0bWVudC5zZXRWYWx1ZSgnJyk7XG4gICAgdGhpcy5ncm91cC5jb250cm9scy5yZWdpb24uc2V0VmFsdWUoJycpO1xuICAgIHRoaXMuZ3JvdXAuY29udHJvbHMuY291bnRyeS5zZXRWYWx1ZSgnJyk7XG5cbiAgICBwbGFjZS5hZGRyZXNzX2NvbXBvbmVudHMuZm9yRWFjaChhZGRyZXNzX2NvbXBvbmVudCA9PiB7XG4gICAgICBpZiAoYWRkcmVzc19jb21wb25lbnQudHlwZXMuaW5kZXhPZignbG9jYWxpdHknKSA+PSAwKSB7XG4gICAgICAgIHRoaXMuZ3JvdXAuY29udHJvbHMuY2l0eS5zZXRWYWx1ZShhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWUpO1xuICAgICAgfVxuICAgICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzLmluZGV4T2YoJ2FkbWluaXN0cmF0aXZlX2FyZWFfbGV2ZWxfMicpID49IDApIHtcbiAgICAgICAgdGhpcy5ncm91cC5jb250cm9scy5kZXBhcnRtZW50LnNldFZhbHVlKGFkZHJlc3NfY29tcG9uZW50LmxvbmdfbmFtZSk7XG4gICAgICB9XG4gICAgICBpZiAoYWRkcmVzc19jb21wb25lbnQudHlwZXMuaW5kZXhPZignYWRtaW5pc3RyYXRpdmVfYXJlYV9sZXZlbF8xJykgPj0gMCkge1xuICAgICAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLnJlZ2lvbi5zZXRWYWx1ZShhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWUpO1xuICAgICAgfVxuICAgICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzLmluZGV4T2YoJ2NvdW50cnknKSA+PSAwKSB7XG4gICAgICAgIHRoaXMuZ3JvdXAuY29udHJvbHMuY291bnRyeS5zZXRWYWx1ZShhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWUpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgdGhpcy5ncm91cC5jb250cm9scy5sYXQuc2V0VmFsdWUocGxhY2UuZ2VvbWV0cnkubG9jYXRpb24ubGF0KCkpO1xuICAgIHRoaXMuZ3JvdXAuY29udHJvbHMubG9uLnNldFZhbHVlKHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLmxuZygpKTtcbiAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmFkZHJlc3Muc2V0VmFsdWUocGxhY2UuZm9ybWF0dGVkX2FkZHJlc3MpO1xuICB9XG5cbn1cbiJdfQ==