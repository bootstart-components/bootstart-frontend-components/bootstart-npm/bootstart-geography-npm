/// <reference types="googlemaps" />
import { BootstartGeographyFormGroup } from '../../models/bootstart-geography-form-group';
export declare class BootstartGeographyQuestionComponent {
    /** Form group */
    formGroup: BootstartGeographyFormGroup;
    /** Optional question label. Translation is applied. */
    questionLabel: string;
    /** Optional question icon (Angular Material icon) */
    questionIcon: string;
    /** Optional placeholder */
    questionPlaceholder: string;
    /** Is the question required? */
    required: boolean;
    /** Is the question displayed with a width of 100%? True by default. */
    fullWidth: boolean;
    constructor();
    updatePlace(place: google.maps.places.PlaceResult): void;
    getPlaceholder(): string;
}
