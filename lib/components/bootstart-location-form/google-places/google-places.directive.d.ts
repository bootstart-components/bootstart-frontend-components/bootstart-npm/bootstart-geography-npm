/// <reference types="googlemaps" />
import { ElementRef, EventEmitter, OnInit } from '@angular/core';
export declare class GooglePlacesDirective implements OnInit {
    private elRef;
    selectionChange: EventEmitter<google.maps.places.PlaceResult>;
    private element;
    constructor(elRef: ElementRef);
    ngOnInit(): void;
}
