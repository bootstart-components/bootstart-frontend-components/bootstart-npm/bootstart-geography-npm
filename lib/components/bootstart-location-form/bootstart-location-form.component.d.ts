/// <reference types="googlemaps" />
import { OnInit } from '@angular/core';
import { BootstartLocationFormGroup } from '../../models/form/bootstart-location-form-group';
import { BootstartLocationFormOptions } from '../../models/form/bootstart-location-form-options';
export declare class BootstartLocationFormComponent implements OnInit {
    /** Associated form group */
    group: BootstartLocationFormGroup;
    /** Set of options */
    options: BootstartLocationFormOptions;
    constructor();
    ngOnInit(): void;
    private _setDefaultOptions();
    /** Place update */
    updatePlace(place: google.maps.places.PlaceResult): void;
}
