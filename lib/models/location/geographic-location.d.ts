import { GeographicLocationCoordinates } from './geographic-location-coordinates';
export interface GeographicLocation {
    /** City */
    city: string;
    /** Department (administrative area level 1) */
    department: string;
    /** Region (administrative area level 2) */
    region: string;
    /** Country */
    country: string;
    /** Full address */
    address: string;
    /** Geographic coordinates */
    coordinates: GeographicLocationCoordinates;
}
