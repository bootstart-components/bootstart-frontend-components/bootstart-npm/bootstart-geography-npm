export interface BootstartLocationFormOptions {
    /** Question placeholder */
    label?: string;
    /** Optional question icon (Angular Material icon) */
    icon?: string;
    /** Is the question required? */
    required?: boolean;
    /** Question field's width in percent */
    width?: number;
}
