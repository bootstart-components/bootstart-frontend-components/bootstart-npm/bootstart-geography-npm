import { AbstractControlOptions, AsyncValidatorFn, FormControl, FormGroup, ValidatorFn } from '@angular/forms';
import { GeographicLocation } from './geographic-location';
export declare class BootstartGeographyFormGroup extends FormGroup {
    constructor(validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions | any, asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null);
    /** Form controls */
    controls: {
        city: FormControl;
        department: FormControl;
        region: FormControl;
        country: FormControl;
        address: FormControl;
        lat: FormControl;
        lon: FormControl;
    };
    /**
     * Form controls loader.
     * @param geographicLocation Object that contains information about the geographic location.
     */
    loadControls(geographicLocation: GeographicLocation): void;
    /** Form controls saver into a geographic location. */
    saveControls(): GeographicLocation;
}
