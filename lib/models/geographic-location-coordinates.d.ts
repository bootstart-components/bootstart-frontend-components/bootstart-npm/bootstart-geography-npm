export interface GeographicLocationCoordinates {
    /** Latitude */
    lat: number;
    /** Longitude */
    lon: number;
}
