(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/material'), require('@angular/common'), require('@angular/forms'), require('@ngx-translate/core')) :
    typeof define === 'function' && define.amd ? define('bootstart-geography', ['exports', '@angular/core', '@angular/material', '@angular/common', '@angular/forms', '@ngx-translate/core'], factory) :
    (factory((global['bootstart-geography'] = {}),global.ng.core,global.ng.material,global.ng.common,global.ng.forms,null));
}(this, (function (exports,core,material,common,forms,core$1) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartLocationFormComponent = (function () {
        function BootstartLocationFormComponent() {
        }
        /**
         * @return {?}
         */
        BootstartLocationFormComponent.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                this._setDefaultOptions();
            };
        /**
         * @return {?}
         */
        BootstartLocationFormComponent.prototype._setDefaultOptions = /**
         * @return {?}
         */
            function () {
                if (this.options.width === undefined) {
                    this.options.width = 100;
                }
            };
        /** Place update */
        /**
         * Place update
         * @param {?} place
         * @return {?}
         */
        BootstartLocationFormComponent.prototype.updatePlace = /**
         * Place update
         * @param {?} place
         * @return {?}
         */
            function (place) {
                var _this = this;
                this.group.controls.city.setValue('');
                this.group.controls.department.setValue('');
                this.group.controls.region.setValue('');
                this.group.controls.country.setValue('');
                place.address_components.forEach(function (address_component) {
                    if (address_component.types.indexOf('locality') >= 0) {
                        _this.group.controls.city.setValue(address_component.long_name);
                    }
                    if (address_component.types.indexOf('administrative_area_level_2') >= 0) {
                        _this.group.controls.department.setValue(address_component.long_name);
                    }
                    if (address_component.types.indexOf('administrative_area_level_1') >= 0) {
                        _this.group.controls.region.setValue(address_component.long_name);
                    }
                    if (address_component.types.indexOf('country') >= 0) {
                        _this.group.controls.country.setValue(address_component.long_name);
                    }
                });
                this.group.controls.lat.setValue(place.geometry.location.lat());
                this.group.controls.lon.setValue(place.geometry.location.lng());
                this.group.controls.address.setValue(place.formatted_address);
            };
        BootstartLocationFormComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'bootstart-location-form',
                        template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{ options.label | translate}}</span>\n  </mat-label>\n\n  <input matInput appGooglePlaces\n         type=\"text\"\n         [placeholder]=\"this.group.controls.address.value\"\n         (selectionChange)=\"updatePlace($event)\"\n         [required]=\"options.required\">\n</mat-form-field>\n"
                    },] },
        ];
        /** @nocollapse */
        BootstartLocationFormComponent.ctorParameters = function () { return []; };
        BootstartLocationFormComponent.propDecorators = {
            group: [{ type: core.Input }],
            options: [{ type: core.Input }]
        };
        return BootstartLocationFormComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var GooglePlacesDirective = (function () {
        function GooglePlacesDirective(elRef) {
            this.elRef = elRef;
            this.selectionChange = new core.EventEmitter();
            this.element = elRef.nativeElement;
        }
        /**
         * @return {?}
         */
        GooglePlacesDirective.prototype.ngOnInit = /**
         * @return {?}
         */
            function () {
                var _this = this;
                /** @type {?} */
                var autocomplete = new google.maps.places.Autocomplete(this.element);
                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    _this.selectionChange.emit(autocomplete.getPlace());
                });
            };
        GooglePlacesDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: '[appGooglePlaces]'
                    },] },
        ];
        /** @nocollapse */
        GooglePlacesDirective.ctorParameters = function () {
            return [
                { type: core.ElementRef }
            ];
        };
        GooglePlacesDirective.propDecorators = {
            selectionChange: [{ type: core.Output }]
        };
        return GooglePlacesDirective;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartGeographyModule = (function () {
        function BootstartGeographyModule() {
        }
        BootstartGeographyModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            common.CommonModule,
                            forms.FormsModule,
                            forms.ReactiveFormsModule,
                            material.MatFormFieldModule,
                            material.MatInputModule,
                            material.MatIconModule,
                            core$1.TranslateModule
                        ],
                        declarations: [
                            BootstartLocationFormComponent,
                            GooglePlacesDirective
                        ],
                        exports: [
                            BootstartLocationFormComponent,
                            GooglePlacesDirective
                        ],
                        providers: [],
                    },] },
        ];
        return BootstartGeographyModule;
    }());

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (b.hasOwnProperty(p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */
    var BootstartLocationFormGroup = (function (_super) {
        __extends(BootstartLocationFormGroup, _super);
        function BootstartLocationFormGroup(validatorOrOpts, asyncValidator) {
            var _this = this;
            /** @type {?} */
            var builder = new forms.FormBuilder();
            /** @type {?} */
            var controls = {
                city: builder.control(''),
                department: builder.control(''),
                region: builder.control(''),
                country: builder.control(''),
                address: builder.control(''),
                lat: builder.control(0),
                lon: builder.control(0),
            };
            _this = _super.call(this, controls, validatorOrOpts, asyncValidator) || this;
            _this.controls = controls;
            return _this;
        }
        /**
         * Form controls loader.
         * @param geographicLocation Object that contains information about the geographic location.
         */
        /**
         * Form controls loader.
         * @param {?} geographicLocation Object that contains information about the geographic location.
         * @return {?}
         */
        BootstartLocationFormGroup.prototype.loadControls = /**
         * Form controls loader.
         * @param {?} geographicLocation Object that contains information about the geographic location.
         * @return {?}
         */
            function (geographicLocation) {
                this.controls.city.setValue(geographicLocation.city);
                this.controls.department.setValue(geographicLocation.department);
                this.controls.region.setValue(geographicLocation.region);
                this.controls.country.setValue(geographicLocation.country);
                this.controls.address.setValue(geographicLocation.address);
                if (geographicLocation.coordinates) {
                    this.controls.lat.setValue(geographicLocation.coordinates.lat);
                    this.controls.lon.setValue(geographicLocation.coordinates.lon);
                }
            };
        /** Form controls saver into a geographic location. */
        /**
         * Form controls saver into a geographic location.
         * @return {?}
         */
        BootstartLocationFormGroup.prototype.saveControls = /**
         * Form controls saver into a geographic location.
         * @return {?}
         */
            function () {
                return {
                    city: this.controls.city.value,
                    department: this.controls.department.value,
                    region: this.controls.region.value,
                    country: this.controls.country.value,
                    address: this.controls.address.value,
                    coordinates: {
                        lat: this.controls.lat.value,
                        lon: this.controls.lon.value
                    }
                };
            };
        return BootstartLocationFormGroup;
    }(forms.FormGroup));

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
     */

    exports.BootstartGeographyModule = BootstartGeographyModule;
    exports.BootstartLocationFormGroup = BootstartLocationFormGroup;
    exports.ɵa = BootstartLocationFormComponent;
    exports.ɵb = GooglePlacesDirective;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWdlb2dyYXBoeS51bWQuanMubWFwIiwic291cmNlcyI6WyJuZzovL2Jvb3RzdGFydC1nZW9ncmFwaHkvbGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0vYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0uY29tcG9uZW50LnRzIiwibmc6Ly9ib290c3RhcnQtZ2VvZ3JhcGh5L2xpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtL2dvb2dsZS1wbGFjZXMvZ29vZ2xlLXBsYWNlcy5kaXJlY3RpdmUudHMiLCJuZzovL2Jvb3RzdGFydC1nZW9ncmFwaHkvbGliL2Jvb3RzdGFydC1nZW9ncmFwaHkubW9kdWxlLnRzIixudWxsLCJuZzovL2Jvb3RzdGFydC1nZW9ncmFwaHkvbGliL21vZGVscy9mb3JtL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtLWdyb3VwLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbnB1dCwgT25Jbml0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Z29vZ2xlfSBmcm9tICdnb29nbGUtbWFwcyc7XG5pbXBvcnQge0Jvb3RzdGFydExvY2F0aW9uRm9ybUdyb3VwfSBmcm9tICcuLi8uLi9tb2RlbHMvZm9ybS9ib290c3RhcnQtbG9jYXRpb24tZm9ybS1ncm91cCc7XG5pbXBvcnQge0Jvb3RzdGFydExvY2F0aW9uRm9ybU9wdGlvbnN9IGZyb20gJy4uLy4uL21vZGVscy9mb3JtL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtLW9wdGlvbnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICAgICAgICAgICBzZWxlY3RvciAgIDogJ2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtZm9ybS1maWVsZCBbbmdTdHlsZV09XCJ7J3dpZHRoJyA6IG9wdGlvbnMud2lkdGgrJyUnfVwiPlxuICA8bWF0LWxhYmVsPlxuICAgIDxtYXQtaWNvbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPnt7b3B0aW9ucy5pY29ufX08L21hdC1pY29uPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5pY29uXCI+Jm5ic3A7PC9zcGFuPlxuICAgIDxzcGFuICpuZ0lmPVwib3B0aW9ucy5sYWJlbFwiPnt7IG9wdGlvbnMubGFiZWwgfCB0cmFuc2xhdGV9fTwvc3Bhbj5cbiAgPC9tYXQtbGFiZWw+XG5cbiAgPGlucHV0IG1hdElucHV0IGFwcEdvb2dsZVBsYWNlc1xuICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgW3BsYWNlaG9sZGVyXT1cInRoaXMuZ3JvdXAuY29udHJvbHMuYWRkcmVzcy52YWx1ZVwiXG4gICAgICAgICAoc2VsZWN0aW9uQ2hhbmdlKT1cInVwZGF0ZVBsYWNlKCRldmVudClcIlxuICAgICAgICAgW3JlcXVpcmVkXT1cIm9wdGlvbnMucmVxdWlyZWRcIj5cbjwvbWF0LWZvcm0tZmllbGQ+XG5gXG4gICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0TG9jYXRpb25Gb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAvKiogQXNzb2NpYXRlZCBmb3JtIGdyb3VwICovXG4gIEBJbnB1dCgpIGdyb3VwOiBCb290c3RhcnRMb2NhdGlvbkZvcm1Hcm91cDtcblxuICAvKiogU2V0IG9mIG9wdGlvbnMgKi9cbiAgQElucHV0KCkgb3B0aW9uczogQm9vdHN0YXJ0TG9jYXRpb25Gb3JtT3B0aW9ucztcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICAgIHRoaXMuX3NldERlZmF1bHRPcHRpb25zKCk7XG4gIH1cblxuICBwcml2YXRlIF9zZXREZWZhdWx0T3B0aW9ucygpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zLndpZHRoID09PSB1bmRlZmluZWQpIHtcbiAgICAgIHRoaXMub3B0aW9ucy53aWR0aCA9IDEwMDtcbiAgICB9XG4gIH1cblxuICAvKiogUGxhY2UgdXBkYXRlICovXG4gIHVwZGF0ZVBsYWNlKHBsYWNlOiBnb29nbGUubWFwcy5wbGFjZXMuUGxhY2VSZXN1bHQpIHtcbiAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmNpdHkuc2V0VmFsdWUoJycpO1xuICAgIHRoaXMuZ3JvdXAuY29udHJvbHMuZGVwYXJ0bWVudC5zZXRWYWx1ZSgnJyk7XG4gICAgdGhpcy5ncm91cC5jb250cm9scy5yZWdpb24uc2V0VmFsdWUoJycpO1xuICAgIHRoaXMuZ3JvdXAuY29udHJvbHMuY291bnRyeS5zZXRWYWx1ZSgnJyk7XG5cbiAgICBwbGFjZS5hZGRyZXNzX2NvbXBvbmVudHMuZm9yRWFjaChhZGRyZXNzX2NvbXBvbmVudCA9PiB7XG4gICAgICBpZiAoYWRkcmVzc19jb21wb25lbnQudHlwZXMuaW5kZXhPZignbG9jYWxpdHknKSA+PSAwKSB7XG4gICAgICAgIHRoaXMuZ3JvdXAuY29udHJvbHMuY2l0eS5zZXRWYWx1ZShhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWUpO1xuICAgICAgfVxuICAgICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzLmluZGV4T2YoJ2FkbWluaXN0cmF0aXZlX2FyZWFfbGV2ZWxfMicpID49IDApIHtcbiAgICAgICAgdGhpcy5ncm91cC5jb250cm9scy5kZXBhcnRtZW50LnNldFZhbHVlKGFkZHJlc3NfY29tcG9uZW50LmxvbmdfbmFtZSk7XG4gICAgICB9XG4gICAgICBpZiAoYWRkcmVzc19jb21wb25lbnQudHlwZXMuaW5kZXhPZignYWRtaW5pc3RyYXRpdmVfYXJlYV9sZXZlbF8xJykgPj0gMCkge1xuICAgICAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLnJlZ2lvbi5zZXRWYWx1ZShhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWUpO1xuICAgICAgfVxuICAgICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzLmluZGV4T2YoJ2NvdW50cnknKSA+PSAwKSB7XG4gICAgICAgIHRoaXMuZ3JvdXAuY29udHJvbHMuY291bnRyeS5zZXRWYWx1ZShhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWUpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgdGhpcy5ncm91cC5jb250cm9scy5sYXQuc2V0VmFsdWUocGxhY2UuZ2VvbWV0cnkubG9jYXRpb24ubGF0KCkpO1xuICAgIHRoaXMuZ3JvdXAuY29udHJvbHMubG9uLnNldFZhbHVlKHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLmxuZygpKTtcbiAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmFkZHJlc3Muc2V0VmFsdWUocGxhY2UuZm9ybWF0dGVkX2FkZHJlc3MpO1xuICB9XG5cbn1cbiIsImltcG9ydCB7RGlyZWN0aXZlLCBFbGVtZW50UmVmLCBFdmVudEVtaXR0ZXIsIE9uSW5pdCwgT3V0cHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IGdvb2dsZSB9IGZyb20gJ2dvb2dsZS1tYXBzJztcblxuQERpcmVjdGl2ZSh7XG4gICAgICAgICAgICAgc2VsZWN0b3I6ICdbYXBwR29vZ2xlUGxhY2VzXSdcbiAgICAgICAgICAgfSlcbmV4cG9ydCBjbGFzcyBHb29nbGVQbGFjZXNEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQge1xuICBAT3V0cHV0KCkgc2VsZWN0aW9uQ2hhbmdlOiBFdmVudEVtaXR0ZXI8Z29vZ2xlLm1hcHMucGxhY2VzLlBsYWNlUmVzdWx0PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgcHJpdmF0ZSBlbGVtZW50OiBIVE1MSW5wdXRFbGVtZW50O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZWxSZWY6IEVsZW1lbnRSZWYpIHtcbiAgICB0aGlzLmVsZW1lbnQgPSBlbFJlZi5uYXRpdmVFbGVtZW50O1xuICB9XG5cbiAgbmdPbkluaXQoKSB7XG4gICAgY29uc3QgYXV0b2NvbXBsZXRlID0gbmV3IGdvb2dsZS5tYXBzLnBsYWNlcy5BdXRvY29tcGxldGUodGhpcy5lbGVtZW50KTtcbiAgICBnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcihhdXRvY29tcGxldGUsICdwbGFjZV9jaGFuZ2VkJywgKCkgPT4ge1xuICAgICAgdGhpcy5zZWxlY3Rpb25DaGFuZ2UuZW1pdChhdXRvY29tcGxldGUuZ2V0UGxhY2UoKSk7XG4gICAgfSk7XG4gIH1cblxufVxuIiwiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jvb3RzdGFydExvY2F0aW9uRm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQge01hdEZvcm1GaWVsZE1vZHVsZSwgTWF0SWNvbk1vZHVsZSwgTWF0SW5wdXRNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtUcmFuc2xhdGVNb2R1bGV9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHtHb29nbGVQbGFjZXNEaXJlY3RpdmV9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtbG9jYXRpb24tZm9ybS9nb29nbGUtcGxhY2VzL2dvb2dsZS1wbGFjZXMuZGlyZWN0aXZlJztcblxuQE5nTW9kdWxlKHtcbiAgICAgICAgICAgIGltcG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgICAgICAgIEZvcm1zTW9kdWxlLFxuICAgICAgICAgICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRGb3JtRmllbGRNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdElucHV0TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRJY29uTW9kdWxlLFxuICAgICAgICAgICAgICBUcmFuc2xhdGVNb2R1bGVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0TG9jYXRpb25Gb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBHb29nbGVQbGFjZXNEaXJlY3RpdmVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBleHBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0TG9jYXRpb25Gb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBHb29nbGVQbGFjZXNEaXJlY3RpdmVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBwcm92aWRlcnMgICA6IFtdLFxuICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0R2VvZ3JhcGh5TW9kdWxlIHtcbn1cbiIsIi8qISAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG5Db3B5cmlnaHQgKGMpIE1pY3Jvc29mdCBDb3Jwb3JhdGlvbi4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlXHJcbnRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlXHJcbkxpY2Vuc2UgYXQgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5USElTIENPREUgSVMgUFJPVklERUQgT04gQU4gKkFTIElTKiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXHJcbktJTkQsIEVJVEhFUiBFWFBSRVNTIE9SIElNUExJRUQsIElOQ0xVRElORyBXSVRIT1VUIExJTUlUQVRJT04gQU5ZIElNUExJRURcclxuV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIFRJVExFLCBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSxcclxuTUVSQ0hBTlRBQkxJVFkgT1IgTk9OLUlORlJJTkdFTUVOVC5cclxuXHJcblNlZSB0aGUgQXBhY2hlIFZlcnNpb24gMi4wIExpY2Vuc2UgZm9yIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9uc1xyXG5hbmQgbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqICovXHJcbi8qIGdsb2JhbCBSZWZsZWN0LCBQcm9taXNlICovXHJcblxyXG52YXIgZXh0ZW5kU3RhdGljcyA9IGZ1bmN0aW9uKGQsIGIpIHtcclxuICAgIGV4dGVuZFN0YXRpY3MgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHxcclxuICAgICAgICAoeyBfX3Byb3RvX186IFtdIH0gaW5zdGFuY2VvZiBBcnJheSAmJiBmdW5jdGlvbiAoZCwgYikgeyBkLl9fcHJvdG9fXyA9IGI7IH0pIHx8XHJcbiAgICAgICAgZnVuY3Rpb24gKGQsIGIpIHsgZm9yICh2YXIgcCBpbiBiKSBpZiAoYi5oYXNPd25Qcm9wZXJ0eShwKSkgZFtwXSA9IGJbcF07IH07XHJcbiAgICByZXR1cm4gZXh0ZW5kU3RhdGljcyhkLCBiKTtcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2V4dGVuZHMoZCwgYikge1xyXG4gICAgZXh0ZW5kU3RhdGljcyhkLCBiKTtcclxuICAgIGZ1bmN0aW9uIF9fKCkgeyB0aGlzLmNvbnN0cnVjdG9yID0gZDsgfVxyXG4gICAgZC5wcm90b3R5cGUgPSBiID09PSBudWxsID8gT2JqZWN0LmNyZWF0ZShiKSA6IChfXy5wcm90b3R5cGUgPSBiLnByb3RvdHlwZSwgbmV3IF9fKCkpO1xyXG59XHJcblxyXG5leHBvcnQgdmFyIF9fYXNzaWduID0gZnVuY3Rpb24oKSB7XHJcbiAgICBfX2Fzc2lnbiA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gX19hc3NpZ24odCkge1xyXG4gICAgICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG4gICAgICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xyXG4gICAgICAgICAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkpIHRbcF0gPSBzW3BdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdDtcclxuICAgIH1cclxuICAgIHJldHVybiBfX2Fzc2lnbi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19yZXN0KHMsIGUpIHtcclxuICAgIHZhciB0ID0ge307XHJcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcclxuICAgICAgICB0W3BdID0gc1twXTtcclxuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcclxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMClcclxuICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XHJcbiAgICByZXR1cm4gdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcclxuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xyXG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcclxuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3BhcmFtKHBhcmFtSW5kZXgsIGRlY29yYXRvcikge1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQsIGtleSkgeyBkZWNvcmF0b3IodGFyZ2V0LCBrZXksIHBhcmFtSW5kZXgpOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX21ldGFkYXRhKG1ldGFkYXRhS2V5LCBtZXRhZGF0YVZhbHVlKSB7XHJcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QubWV0YWRhdGEgPT09IFwiZnVuY3Rpb25cIikgcmV0dXJuIFJlZmxlY3QubWV0YWRhdGEobWV0YWRhdGFLZXksIG1ldGFkYXRhVmFsdWUpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hd2FpdGVyKHRoaXNBcmcsIF9hcmd1bWVudHMsIFAsIGdlbmVyYXRvcikge1xyXG4gICAgcmV0dXJuIG5ldyAoUCB8fCAoUCA9IFByb21pc2UpKShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XHJcbiAgICAgICAgZnVuY3Rpb24gZnVsZmlsbGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yLm5leHQodmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxyXG4gICAgICAgIGZ1bmN0aW9uIHJlamVjdGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yW1widGhyb3dcIl0odmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxyXG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAocmVzdWx0KSB7IHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZShyZXN1bHQudmFsdWUpOyB9KS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpOyB9XHJcbiAgICAgICAgc3RlcCgoZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pKS5uZXh0KCkpO1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2dlbmVyYXRvcih0aGlzQXJnLCBib2R5KSB7XHJcbiAgICB2YXIgXyA9IHsgbGFiZWw6IDAsIHNlbnQ6IGZ1bmN0aW9uKCkgeyBpZiAodFswXSAmIDEpIHRocm93IHRbMV07IHJldHVybiB0WzFdOyB9LCB0cnlzOiBbXSwgb3BzOiBbXSB9LCBmLCB5LCB0LCBnO1xyXG4gICAgcmV0dXJuIGcgPSB7IG5leHQ6IHZlcmIoMCksIFwidGhyb3dcIjogdmVyYigxKSwgXCJyZXR1cm5cIjogdmVyYigyKSB9LCB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgKGdbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSksIGc7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgcmV0dXJuIGZ1bmN0aW9uICh2KSB7IHJldHVybiBzdGVwKFtuLCB2XSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHN0ZXAob3ApIHtcclxuICAgICAgICBpZiAoZikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkdlbmVyYXRvciBpcyBhbHJlYWR5IGV4ZWN1dGluZy5cIik7XHJcbiAgICAgICAgd2hpbGUgKF8pIHRyeSB7XHJcbiAgICAgICAgICAgIGlmIChmID0gMSwgeSAmJiAodCA9IG9wWzBdICYgMiA/IHlbXCJyZXR1cm5cIl0gOiBvcFswXSA/IHlbXCJ0aHJvd1wiXSB8fCAoKHQgPSB5W1wicmV0dXJuXCJdKSAmJiB0LmNhbGwoeSksIDApIDogeS5uZXh0KSAmJiAhKHQgPSB0LmNhbGwoeSwgb3BbMV0pKS5kb25lKSByZXR1cm4gdDtcclxuICAgICAgICAgICAgaWYgKHkgPSAwLCB0KSBvcCA9IFtvcFswXSAmIDIsIHQudmFsdWVdO1xyXG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDA6IGNhc2UgMTogdCA9IG9wOyBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgNDogXy5sYWJlbCsrOyByZXR1cm4geyB2YWx1ZTogb3BbMV0sIGRvbmU6IGZhbHNlIH07XHJcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDc6IG9wID0gXy5vcHMucG9wKCk7IF8udHJ5cy5wb3AoKTsgY29udGludWU7XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gMyAmJiAoIXQgfHwgKG9wWzFdID4gdFswXSAmJiBvcFsxXSA8IHRbM10pKSkgeyBfLmxhYmVsID0gb3BbMV07IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSA2ICYmIF8ubGFiZWwgPCB0WzFdKSB7IF8ubGFiZWwgPSB0WzFdOyB0ID0gb3A7IGJyZWFrOyB9XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0WzJdKSBfLm9wcy5wb3AoKTtcclxuICAgICAgICAgICAgICAgICAgICBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIG9wID0gYm9keS5jYWxsKHRoaXNBcmcsIF8pO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHsgb3AgPSBbNiwgZV07IHkgPSAwOyB9IGZpbmFsbHkgeyBmID0gdCA9IDA7IH1cclxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fZXhwb3J0U3RhcihtLCBleHBvcnRzKSB7XHJcbiAgICBmb3IgKHZhciBwIGluIG0pIGlmICghZXhwb3J0cy5oYXNPd25Qcm9wZXJ0eShwKSkgZXhwb3J0c1twXSA9IG1bcF07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3ZhbHVlcyhvKSB7XHJcbiAgICB2YXIgbSA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvW1N5bWJvbC5pdGVyYXRvcl0sIGkgPSAwO1xyXG4gICAgaWYgKG0pIHJldHVybiBtLmNhbGwobyk7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIG5leHQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgaWYgKG8gJiYgaSA+PSBvLmxlbmd0aCkgbyA9IHZvaWQgMDtcclxuICAgICAgICAgICAgcmV0dXJuIHsgdmFsdWU6IG8gJiYgb1tpKytdLCBkb25lOiAhbyB9O1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3JlYWQobywgbikge1xyXG4gICAgdmFyIG0gPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb1tTeW1ib2wuaXRlcmF0b3JdO1xyXG4gICAgaWYgKCFtKSByZXR1cm4gbztcclxuICAgIHZhciBpID0gbS5jYWxsKG8pLCByLCBhciA9IFtdLCBlO1xyXG4gICAgdHJ5IHtcclxuICAgICAgICB3aGlsZSAoKG4gPT09IHZvaWQgMCB8fCBuLS0gPiAwKSAmJiAhKHIgPSBpLm5leHQoKSkuZG9uZSkgYXIucHVzaChyLnZhbHVlKTtcclxuICAgIH1cclxuICAgIGNhdGNoIChlcnJvcikgeyBlID0geyBlcnJvcjogZXJyb3IgfTsgfVxyXG4gICAgZmluYWxseSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKHIgJiYgIXIuZG9uZSAmJiAobSA9IGlbXCJyZXR1cm5cIl0pKSBtLmNhbGwoaSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZpbmFsbHkgeyBpZiAoZSkgdGhyb3cgZS5lcnJvcjsgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGFyO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19zcHJlYWQoKSB7XHJcbiAgICBmb3IgKHZhciBhciA9IFtdLCBpID0gMDsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKylcclxuICAgICAgICBhciA9IGFyLmNvbmNhdChfX3JlYWQoYXJndW1lbnRzW2ldKSk7XHJcbiAgICByZXR1cm4gYXI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2F3YWl0KHYpIHtcclxuICAgIHJldHVybiB0aGlzIGluc3RhbmNlb2YgX19hd2FpdCA/ICh0aGlzLnYgPSB2LCB0aGlzKSA6IG5ldyBfX2F3YWl0KHYpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY0dlbmVyYXRvcih0aGlzQXJnLCBfYXJndW1lbnRzLCBnZW5lcmF0b3IpIHtcclxuICAgIGlmICghU3ltYm9sLmFzeW5jSXRlcmF0b3IpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJTeW1ib2wuYXN5bmNJdGVyYXRvciBpcyBub3QgZGVmaW5lZC5cIik7XHJcbiAgICB2YXIgZyA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSwgaSwgcSA9IFtdO1xyXG4gICAgcmV0dXJuIGkgPSB7fSwgdmVyYihcIm5leHRcIiksIHZlcmIoXCJ0aHJvd1wiKSwgdmVyYihcInJldHVyblwiKSwgaVtTeW1ib2wuYXN5bmNJdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IGlmIChnW25dKSBpW25dID0gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChhLCBiKSB7IHEucHVzaChbbiwgdiwgYSwgYl0pID4gMSB8fCByZXN1bWUobiwgdik7IH0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiByZXN1bWUobiwgdikgeyB0cnkgeyBzdGVwKGdbbl0odikpOyB9IGNhdGNoIChlKSB7IHNldHRsZShxWzBdWzNdLCBlKTsgfSB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKHIpIHsgci52YWx1ZSBpbnN0YW5jZW9mIF9fYXdhaXQgPyBQcm9taXNlLnJlc29sdmUoci52YWx1ZS52KS50aGVuKGZ1bGZpbGwsIHJlamVjdCkgOiBzZXR0bGUocVswXVsyXSwgcik7IH1cclxuICAgIGZ1bmN0aW9uIGZ1bGZpbGwodmFsdWUpIHsgcmVzdW1lKFwibmV4dFwiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHJlamVjdCh2YWx1ZSkgeyByZXN1bWUoXCJ0aHJvd1wiLCB2YWx1ZSk7IH1cclxuICAgIGZ1bmN0aW9uIHNldHRsZShmLCB2KSB7IGlmIChmKHYpLCBxLnNoaWZ0KCksIHEubGVuZ3RoKSByZXN1bWUocVswXVswXSwgcVswXVsxXSk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXN5bmNEZWxlZ2F0b3Iobykge1xyXG4gICAgdmFyIGksIHA7XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIsIGZ1bmN0aW9uIChlKSB7IHRocm93IGU7IH0pLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuLCBmKSB7IGlbbl0gPSBvW25dID8gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIChwID0gIXApID8geyB2YWx1ZTogX19hd2FpdChvW25dKHYpKSwgZG9uZTogbiA9PT0gXCJyZXR1cm5cIiB9IDogZiA/IGYodikgOiB2OyB9IDogZjsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY1ZhbHVlcyhvKSB7XHJcbiAgICBpZiAoIVN5bWJvbC5hc3luY0l0ZXJhdG9yKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3ltYm9sLmFzeW5jSXRlcmF0b3IgaXMgbm90IGRlZmluZWQuXCIpO1xyXG4gICAgdmFyIG0gPSBvW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSwgaTtcclxuICAgIHJldHVybiBtID8gbS5jYWxsKG8pIDogKG8gPSB0eXBlb2YgX192YWx1ZXMgPT09IFwiZnVuY3Rpb25cIiA/IF9fdmFsdWVzKG8pIDogb1tTeW1ib2wuaXRlcmF0b3JdKCksIGkgPSB7fSwgdmVyYihcIm5leHRcIiksIHZlcmIoXCJ0aHJvd1wiKSwgdmVyYihcInJldHVyblwiKSwgaVtTeW1ib2wuYXN5bmNJdGVyYXRvcl0gPSBmdW5jdGlvbiAoKSB7IHJldHVybiB0aGlzOyB9LCBpKTtcclxuICAgIGZ1bmN0aW9uIHZlcmIobikgeyBpW25dID0gb1tuXSAmJiBmdW5jdGlvbiAodikgeyByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkgeyB2ID0gb1tuXSh2KSwgc2V0dGxlKHJlc29sdmUsIHJlamVjdCwgdi5kb25lLCB2LnZhbHVlKTsgfSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHNldHRsZShyZXNvbHZlLCByZWplY3QsIGQsIHYpIHsgUHJvbWlzZS5yZXNvbHZlKHYpLnRoZW4oZnVuY3Rpb24odikgeyByZXNvbHZlKHsgdmFsdWU6IHYsIGRvbmU6IGQgfSk7IH0sIHJlamVjdCk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fbWFrZVRlbXBsYXRlT2JqZWN0KGNvb2tlZCwgcmF3KSB7XHJcbiAgICBpZiAoT2JqZWN0LmRlZmluZVByb3BlcnR5KSB7IE9iamVjdC5kZWZpbmVQcm9wZXJ0eShjb29rZWQsIFwicmF3XCIsIHsgdmFsdWU6IHJhdyB9KTsgfSBlbHNlIHsgY29va2VkLnJhdyA9IHJhdzsgfVxyXG4gICAgcmV0dXJuIGNvb2tlZDtcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2ltcG9ydFN0YXIobW9kKSB7XHJcbiAgICBpZiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSByZXR1cm4gbW9kO1xyXG4gICAgdmFyIHJlc3VsdCA9IHt9O1xyXG4gICAgaWYgKG1vZCAhPSBudWxsKSBmb3IgKHZhciBrIGluIG1vZCkgaWYgKE9iamVjdC5oYXNPd25Qcm9wZXJ0eS5jYWxsKG1vZCwgaykpIHJlc3VsdFtrXSA9IG1vZFtrXTtcclxuICAgIHJlc3VsdC5kZWZhdWx0ID0gbW9kO1xyXG4gICAgcmV0dXJuIHJlc3VsdDtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9faW1wb3J0RGVmYXVsdChtb2QpIHtcclxuICAgIHJldHVybiAobW9kICYmIG1vZC5fX2VzTW9kdWxlKSA/IG1vZCA6IHsgZGVmYXVsdDogbW9kIH07XHJcbn1cclxuIiwiaW1wb3J0IHtcbiAgQWJzdHJhY3RDb250cm9sT3B0aW9ucyxcbiAgQXN5bmNWYWxpZGF0b3JGbixcbiAgRm9ybUJ1aWxkZXIsXG4gIEZvcm1Db250cm9sLFxuICBGb3JtR3JvdXAsXG4gIFZhbGlkYXRvckZuXG59IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7R2VvZ3JhcGhpY0xvY2F0aW9ufSBmcm9tICcuLi9sb2NhdGlvbi9nZW9ncmFwaGljLWxvY2F0aW9uJztcblxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydExvY2F0aW9uRm9ybUdyb3VwIGV4dGVuZHMgRm9ybUdyb3VwIHtcblxuICBjb25zdHJ1Y3Rvcih2YWxpZGF0b3JPck9wdHM/OiBWYWxpZGF0b3JGbiB8IFZhbGlkYXRvckZuW10gfCBBYnN0cmFjdENvbnRyb2xPcHRpb25zIHwgYW55LFxuICAgICAgICAgICAgICBhc3luY1ZhbGlkYXRvcj86IEFzeW5jVmFsaWRhdG9yRm4gfCBBc3luY1ZhbGlkYXRvckZuW10gfCBudWxsKSB7XG4gICAgY29uc3QgYnVpbGRlciA9IG5ldyBGb3JtQnVpbGRlcigpO1xuICAgIGNvbnN0IGNvbnRyb2xzID0ge1xuICAgICAgY2l0eSAgICAgIDogYnVpbGRlci5jb250cm9sKCcnKSxcbiAgICAgIGRlcGFydG1lbnQ6IGJ1aWxkZXIuY29udHJvbCgnJyksXG4gICAgICByZWdpb24gICAgOiBidWlsZGVyLmNvbnRyb2woJycpLFxuICAgICAgY291bnRyeSAgIDogYnVpbGRlci5jb250cm9sKCcnKSxcbiAgICAgIGFkZHJlc3MgICA6IGJ1aWxkZXIuY29udHJvbCgnJyksXG4gICAgICBsYXQgICAgICAgOiBidWlsZGVyLmNvbnRyb2woMCksXG4gICAgICBsb24gICAgICAgOiBidWlsZGVyLmNvbnRyb2woMCksXG4gICAgfTtcbiAgICBzdXBlcihjb250cm9scywgdmFsaWRhdG9yT3JPcHRzLCBhc3luY1ZhbGlkYXRvcik7XG4gICAgdGhpcy5jb250cm9scyA9IGNvbnRyb2xzO1xuICB9XG5cbiAgLyoqIEZvcm0gY29udHJvbHMgKi9cbiAgY29udHJvbHM6IHtcbiAgICBjaXR5OiBGb3JtQ29udHJvbCxcbiAgICBkZXBhcnRtZW50OiBGb3JtQ29udHJvbCxcbiAgICByZWdpb246IEZvcm1Db250cm9sLFxuICAgIGNvdW50cnk6IEZvcm1Db250cm9sLFxuICAgIGFkZHJlc3M6IEZvcm1Db250cm9sLFxuICAgIGxhdDogRm9ybUNvbnRyb2wsXG4gICAgbG9uOiBGb3JtQ29udHJvbFxuICB9O1xuXG4gIC8qKlxuICAgKiBGb3JtIGNvbnRyb2xzIGxvYWRlci5cbiAgICogQHBhcmFtIGdlb2dyYXBoaWNMb2NhdGlvbiBPYmplY3QgdGhhdCBjb250YWlucyBpbmZvcm1hdGlvbiBhYm91dCB0aGUgZ2VvZ3JhcGhpYyBsb2NhdGlvbi5cbiAgICovXG4gIGxvYWRDb250cm9scyhnZW9ncmFwaGljTG9jYXRpb246IEdlb2dyYXBoaWNMb2NhdGlvbikge1xuICAgIHRoaXMuY29udHJvbHMuY2l0eS5zZXRWYWx1ZShnZW9ncmFwaGljTG9jYXRpb24uY2l0eSk7XG4gICAgdGhpcy5jb250cm9scy5kZXBhcnRtZW50LnNldFZhbHVlKGdlb2dyYXBoaWNMb2NhdGlvbi5kZXBhcnRtZW50KTtcbiAgICB0aGlzLmNvbnRyb2xzLnJlZ2lvbi5zZXRWYWx1ZShnZW9ncmFwaGljTG9jYXRpb24ucmVnaW9uKTtcbiAgICB0aGlzLmNvbnRyb2xzLmNvdW50cnkuc2V0VmFsdWUoZ2VvZ3JhcGhpY0xvY2F0aW9uLmNvdW50cnkpO1xuICAgIHRoaXMuY29udHJvbHMuYWRkcmVzcy5zZXRWYWx1ZShnZW9ncmFwaGljTG9jYXRpb24uYWRkcmVzcyk7XG4gICAgaWYgKGdlb2dyYXBoaWNMb2NhdGlvbi5jb29yZGluYXRlcykge1xuICAgICAgdGhpcy5jb250cm9scy5sYXQuc2V0VmFsdWUoZ2VvZ3JhcGhpY0xvY2F0aW9uLmNvb3JkaW5hdGVzLmxhdCk7XG4gICAgICB0aGlzLmNvbnRyb2xzLmxvbi5zZXRWYWx1ZShnZW9ncmFwaGljTG9jYXRpb24uY29vcmRpbmF0ZXMubG9uKTtcbiAgICB9XG4gIH1cblxuICAvKiogRm9ybSBjb250cm9scyBzYXZlciBpbnRvIGEgZ2VvZ3JhcGhpYyBsb2NhdGlvbi4gKi9cbiAgc2F2ZUNvbnRyb2xzKCk6IEdlb2dyYXBoaWNMb2NhdGlvbiB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGNpdHkgICAgICAgOiB0aGlzLmNvbnRyb2xzLmNpdHkudmFsdWUsXG4gICAgICBkZXBhcnRtZW50IDogdGhpcy5jb250cm9scy5kZXBhcnRtZW50LnZhbHVlLFxuICAgICAgcmVnaW9uICAgICA6IHRoaXMuY29udHJvbHMucmVnaW9uLnZhbHVlLFxuICAgICAgY291bnRyeSAgICA6IHRoaXMuY29udHJvbHMuY291bnRyeS52YWx1ZSxcbiAgICAgIGFkZHJlc3MgICAgOiB0aGlzLmNvbnRyb2xzLmFkZHJlc3MudmFsdWUsXG4gICAgICBjb29yZGluYXRlczoge1xuICAgICAgICBsYXQ6IHRoaXMuY29udHJvbHMubGF0LnZhbHVlLFxuICAgICAgICBsb246IHRoaXMuY29udHJvbHMubG9uLnZhbHVlXG4gICAgICB9XG4gICAgfVxuICB9XG5cbn1cbiJdLCJuYW1lcyI6WyJDb21wb25lbnQiLCJJbnB1dCIsIkV2ZW50RW1pdHRlciIsIkRpcmVjdGl2ZSIsIkVsZW1lbnRSZWYiLCJPdXRwdXQiLCJOZ01vZHVsZSIsIkNvbW1vbk1vZHVsZSIsIkZvcm1zTW9kdWxlIiwiUmVhY3RpdmVGb3Jtc01vZHVsZSIsIk1hdEZvcm1GaWVsZE1vZHVsZSIsIk1hdElucHV0TW9kdWxlIiwiTWF0SWNvbk1vZHVsZSIsIlRyYW5zbGF0ZU1vZHVsZSIsInRzbGliXzEuX19leHRlbmRzIiwiRm9ybUJ1aWxkZXIiLCJGb3JtR3JvdXAiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtRQThCRTtTQUNDOzs7O1FBRUQsaURBQVE7OztZQUFSO2dCQUNFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO2FBQzNCOzs7O1FBRU8sMkRBQWtCOzs7O2dCQUN4QixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxLQUFLLFNBQVMsRUFBRTtvQkFDcEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO2lCQUMxQjs7Ozs7Ozs7UUFJSCxvREFBVzs7Ozs7WUFBWCxVQUFZLEtBQXFDO2dCQUFqRCxpQkF3QkM7Z0JBdkJDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3RDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3hDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBRXpDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxpQkFBaUI7b0JBQ2hELElBQUksaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQ3BELEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7cUJBQ2hFO29CQUNELElBQUksaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDdkUsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDdEU7b0JBQ0QsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLDZCQUE2QixDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUN2RSxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO3FCQUNsRTtvQkFDRCxJQUFJLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO3dCQUNuRCxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO3FCQUNuRTtpQkFDRixDQUFDLENBQUM7Z0JBRUgsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO2dCQUNoRSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBQ2hFLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7YUFDL0Q7O29CQS9ERkEsY0FBUyxTQUFDO3dCQUNFLFFBQVEsRUFBSyx5QkFBeUI7d0JBQ3RDLFFBQVEsRUFBRSx1Z0JBYXRCO3FCQUNXOzs7Ozs0QkFJVEMsVUFBSzs4QkFHTEEsVUFBSzs7NkNBNUJSOzs7Ozs7O0FDQUE7UUFVRSwrQkFBb0IsS0FBaUI7WUFBakIsVUFBSyxHQUFMLEtBQUssQ0FBWTttQ0FIcUMsSUFBSUMsaUJBQVksRUFBRTtZQUkxRixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxhQUFhLENBQUM7U0FDcEM7Ozs7UUFFRCx3Q0FBUTs7O1lBQVI7Z0JBQUEsaUJBS0M7O2dCQUpDLElBQU0sWUFBWSxHQUFHLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdkUsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxlQUFlLEVBQUU7b0JBQzNELEtBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO2lCQUNwRCxDQUFDLENBQUM7YUFDSjs7b0JBaEJGQyxjQUFTLFNBQUM7d0JBQ0UsUUFBUSxFQUFFLG1CQUFtQjtxQkFDOUI7Ozs7O3dCQUxPQyxlQUFVOzs7O3NDQU8xQkMsV0FBTTs7b0NBUFQ7Ozs7Ozs7QUNBQTs7OztvQkFRQ0MsYUFBUSxTQUFDO3dCQUNFLE9BQU8sRUFBTzs0QkFDWkMsbUJBQVk7NEJBQ1pDLGlCQUFXOzRCQUNYQyx5QkFBbUI7NEJBQ25CQywyQkFBa0I7NEJBQ2xCQyx1QkFBYzs0QkFDZEMsc0JBQWE7NEJBQ2JDLHNCQUFlO3lCQUNoQjt3QkFDRCxZQUFZLEVBQUU7NEJBQ1osOEJBQThCOzRCQUM5QixxQkFBcUI7eUJBQ3RCO3dCQUNELE9BQU8sRUFBTzs0QkFDWiw4QkFBOEI7NEJBQzlCLHFCQUFxQjt5QkFDdEI7d0JBQ0QsU0FBUyxFQUFLLEVBQUU7cUJBQ2pCOzt1Q0EzQlg7OztJQ0FBOzs7Ozs7Ozs7Ozs7OztJQWNBO0lBRUEsSUFBSSxhQUFhLEdBQUcsVUFBUyxDQUFDLEVBQUUsQ0FBQztRQUM3QixhQUFhLEdBQUcsTUFBTSxDQUFDLGNBQWM7YUFDaEMsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLFlBQVksS0FBSyxJQUFJLFVBQVUsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDNUUsVUFBVSxDQUFDLEVBQUUsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQztnQkFBRSxJQUFJLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO29CQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQy9FLE9BQU8sYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUMvQixDQUFDLENBQUM7QUFFRix1QkFBMEIsQ0FBQyxFQUFFLENBQUM7UUFDMUIsYUFBYSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNwQixnQkFBZ0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsRUFBRTtRQUN2QyxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsS0FBSyxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ3pGLENBQUM7Ozs7OztRQ2pCRDtRQUFnREMsOENBQVM7UUFFdkQsb0NBQVksZUFBNEUsRUFDNUUsY0FBNkQ7WUFEekUsaUJBY0M7O1lBWkMsSUFBTSxPQUFPLEdBQUcsSUFBSUMsaUJBQVcsRUFBRSxDQUFDOztZQUNsQyxJQUFNLFFBQVEsR0FBRztnQkFDZixJQUFJLEVBQVEsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7Z0JBQy9CLFVBQVUsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztnQkFDL0IsTUFBTSxFQUFNLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO2dCQUMvQixPQUFPLEVBQUssT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7Z0JBQy9CLE9BQU8sRUFBSyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztnQkFDL0IsR0FBRyxFQUFTLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dCQUM5QixHQUFHLEVBQVMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7YUFDL0IsQ0FBQztZQUNGLFFBQUEsa0JBQU0sUUFBUSxFQUFFLGVBQWUsRUFBRSxjQUFjLENBQUMsU0FBQztZQUNqRCxLQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQzs7U0FDMUI7Ozs7Ozs7Ozs7UUFpQkQsaURBQVk7Ozs7O1lBQVosVUFBYSxrQkFBc0M7Z0JBQ2pELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNqRSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3pELElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDM0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUMzRCxJQUFJLGtCQUFrQixDQUFDLFdBQVcsRUFBRTtvQkFDbEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDL0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDaEU7YUFDRjs7Ozs7O1FBR0QsaURBQVk7Ozs7WUFBWjtnQkFDRSxPQUFPO29CQUNMLElBQUksRUFBUyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLO29CQUNyQyxVQUFVLEVBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSztvQkFDM0MsTUFBTSxFQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLEtBQUs7b0JBQ3ZDLE9BQU8sRUFBTSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLO29CQUN4QyxPQUFPLEVBQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztvQkFDeEMsV0FBVyxFQUFFO3dCQUNYLEdBQUcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxLQUFLO3dCQUM1QixHQUFHLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSztxQkFDN0I7aUJBQ0YsQ0FBQTthQUNGO3lDQXBFSDtNQVVnREMsZUFBUyxFQTREeEQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=