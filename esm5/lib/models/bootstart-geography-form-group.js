/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { FormBuilder, FormGroup } from '@angular/forms';
var BootstartGeographyFormGroup = /** @class */ (function (_super) {
    tslib_1.__extends(BootstartGeographyFormGroup, _super);
    function BootstartGeographyFormGroup(validatorOrOpts, asyncValidator) {
        var _this = this;
        /** @type {?} */
        var builder = new FormBuilder();
        /** @type {?} */
        var controls = {
            city: builder.control(''),
            department: builder.control(''),
            region: builder.control(''),
            country: builder.control(''),
            address: builder.control(''),
            lat: builder.control(0),
            lon: builder.control(0),
        };
        _this = _super.call(this, controls, validatorOrOpts, asyncValidator) || this;
        _this.controls = controls;
        return _this;
    }
    /**
     * Form controls loader.
     * @param geographicLocation Object that contains information about the geographic location.
     */
    /**
     * Form controls loader.
     * @param {?} geographicLocation Object that contains information about the geographic location.
     * @return {?}
     */
    BootstartGeographyFormGroup.prototype.loadControls = /**
     * Form controls loader.
     * @param {?} geographicLocation Object that contains information about the geographic location.
     * @return {?}
     */
    function (geographicLocation) {
        this.controls.city.setValue(geographicLocation.city);
        this.controls.department.setValue(geographicLocation.department);
        this.controls.region.setValue(geographicLocation.region);
        this.controls.country.setValue(geographicLocation.country);
        this.controls.address.setValue(geographicLocation.address);
        if (geographicLocation.coordinates) {
            this.controls.lat.setValue(geographicLocation.coordinates.lat);
            this.controls.lon.setValue(geographicLocation.coordinates.lon);
        }
    };
    /** Form controls saver into a geographic location. */
    /**
     * Form controls saver into a geographic location.
     * @return {?}
     */
    BootstartGeographyFormGroup.prototype.saveControls = /**
     * Form controls saver into a geographic location.
     * @return {?}
     */
    function () {
        return {
            city: this.controls.city.value,
            department: this.controls.department.value,
            region: this.controls.region.value,
            country: this.controls.country.value,
            address: this.controls.address.value,
            coordinates: {
                lat: this.controls.lat.value,
                lon: this.controls.lon.value
            }
        };
    };
    return BootstartGeographyFormGroup;
}(FormGroup));
export { BootstartGeographyFormGroup };
if (false) {
    /**
     * Form controls
     * @type {?}
     */
    BootstartGeographyFormGroup.prototype.controls;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWdlb2dyYXBoeS1mb3JtLWdyb3VwLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWdlb2dyYXBoeS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvYm9vdHN0YXJ0LWdlb2dyYXBoeS1mb3JtLWdyb3VwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUdMLFdBQVcsRUFFWCxTQUFTLEVBRVYsTUFBTSxnQkFBZ0IsQ0FBQztBQUd4QixJQUFBO0lBQWlELHVEQUFTO0lBRXhELHFDQUFZLGVBQTRFLEVBQzVFLGNBQTZEO1FBRHpFLGlCQWNDOztRQVpDLElBQU0sT0FBTyxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7O1FBQ2xDLElBQU0sUUFBUSxHQUFHO1lBQ2YsSUFBSSxFQUFRLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO1lBQy9CLFVBQVUsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztZQUMvQixNQUFNLEVBQU0sT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7WUFDL0IsT0FBTyxFQUFLLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO1lBQy9CLE9BQU8sRUFBSyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztZQUMvQixHQUFHLEVBQVMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDOUIsR0FBRyxFQUFTLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1NBQy9CLENBQUM7UUFDRixRQUFBLGtCQUFNLFFBQVEsRUFBRSxlQUFlLEVBQUUsY0FBYyxDQUFDLFNBQUM7UUFDakQsS0FBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7O0tBQzFCO0lBYUQ7OztPQUdHOzs7Ozs7SUFDSCxrREFBWTs7Ozs7SUFBWixVQUFhLGtCQUFzQztRQUNqRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNELEVBQUUsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ2hFO0tBQ0Y7SUFFRCxzREFBc0Q7Ozs7O0lBQ3RELGtEQUFZOzs7O0lBQVo7UUFDRSxNQUFNLENBQUM7WUFDTCxJQUFJLEVBQVMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSztZQUNyQyxVQUFVLEVBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSztZQUMzQyxNQUFNLEVBQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSztZQUN2QyxPQUFPLEVBQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUN4QyxPQUFPLEVBQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsS0FBSztZQUN4QyxXQUFXLEVBQUU7Z0JBQ1gsR0FBRyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEtBQUs7Z0JBQzVCLEdBQUcsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxLQUFLO2FBQzdCO1NBQ0YsQ0FBQTtLQUNGO3NDQXBFSDtFQVVpRCxTQUFTLEVBNER6RCxDQUFBO0FBNURELHVDQTREQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gIEFic3RyYWN0Q29udHJvbE9wdGlvbnMsXG4gIEFzeW5jVmFsaWRhdG9yRm4sXG4gIEZvcm1CdWlsZGVyLFxuICBGb3JtQ29udHJvbCxcbiAgRm9ybUdyb3VwLFxuICBWYWxpZGF0b3JGblxufSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQge0dlb2dyYXBoaWNMb2NhdGlvbn0gZnJvbSAnLi9nZW9ncmFwaGljLWxvY2F0aW9uJztcblxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEdlb2dyYXBoeUZvcm1Hcm91cCBleHRlbmRzIEZvcm1Hcm91cCB7XG5cbiAgY29uc3RydWN0b3IodmFsaWRhdG9yT3JPcHRzPzogVmFsaWRhdG9yRm4gfCBWYWxpZGF0b3JGbltdIHwgQWJzdHJhY3RDb250cm9sT3B0aW9ucyB8IGFueSxcbiAgICAgICAgICAgICAgYXN5bmNWYWxpZGF0b3I/OiBBc3luY1ZhbGlkYXRvckZuIHwgQXN5bmNWYWxpZGF0b3JGbltdIHwgbnVsbCkge1xuICAgIGNvbnN0IGJ1aWxkZXIgPSBuZXcgRm9ybUJ1aWxkZXIoKTtcbiAgICBjb25zdCBjb250cm9scyA9IHtcbiAgICAgIGNpdHkgICAgICA6IGJ1aWxkZXIuY29udHJvbCgnJyksXG4gICAgICBkZXBhcnRtZW50OiBidWlsZGVyLmNvbnRyb2woJycpLFxuICAgICAgcmVnaW9uICAgIDogYnVpbGRlci5jb250cm9sKCcnKSxcbiAgICAgIGNvdW50cnkgICA6IGJ1aWxkZXIuY29udHJvbCgnJyksXG4gICAgICBhZGRyZXNzICAgOiBidWlsZGVyLmNvbnRyb2woJycpLFxuICAgICAgbGF0ICAgICAgIDogYnVpbGRlci5jb250cm9sKDApLFxuICAgICAgbG9uICAgICAgIDogYnVpbGRlci5jb250cm9sKDApLFxuICAgIH07XG4gICAgc3VwZXIoY29udHJvbHMsIHZhbGlkYXRvck9yT3B0cywgYXN5bmNWYWxpZGF0b3IpO1xuICAgIHRoaXMuY29udHJvbHMgPSBjb250cm9scztcbiAgfVxuXG4gIC8qKiBGb3JtIGNvbnRyb2xzICovXG4gIGNvbnRyb2xzOiB7XG4gICAgY2l0eTogRm9ybUNvbnRyb2wsXG4gICAgZGVwYXJ0bWVudDogRm9ybUNvbnRyb2wsXG4gICAgcmVnaW9uOiBGb3JtQ29udHJvbCxcbiAgICBjb3VudHJ5OiBGb3JtQ29udHJvbCxcbiAgICBhZGRyZXNzOiBGb3JtQ29udHJvbCxcbiAgICBsYXQ6IEZvcm1Db250cm9sLFxuICAgIGxvbjogRm9ybUNvbnRyb2xcbiAgfTtcblxuICAvKipcbiAgICogRm9ybSBjb250cm9scyBsb2FkZXIuXG4gICAqIEBwYXJhbSBnZW9ncmFwaGljTG9jYXRpb24gT2JqZWN0IHRoYXQgY29udGFpbnMgaW5mb3JtYXRpb24gYWJvdXQgdGhlIGdlb2dyYXBoaWMgbG9jYXRpb24uXG4gICAqL1xuICBsb2FkQ29udHJvbHMoZ2VvZ3JhcGhpY0xvY2F0aW9uOiBHZW9ncmFwaGljTG9jYXRpb24pIHtcbiAgICB0aGlzLmNvbnRyb2xzLmNpdHkuc2V0VmFsdWUoZ2VvZ3JhcGhpY0xvY2F0aW9uLmNpdHkpO1xuICAgIHRoaXMuY29udHJvbHMuZGVwYXJ0bWVudC5zZXRWYWx1ZShnZW9ncmFwaGljTG9jYXRpb24uZGVwYXJ0bWVudCk7XG4gICAgdGhpcy5jb250cm9scy5yZWdpb24uc2V0VmFsdWUoZ2VvZ3JhcGhpY0xvY2F0aW9uLnJlZ2lvbik7XG4gICAgdGhpcy5jb250cm9scy5jb3VudHJ5LnNldFZhbHVlKGdlb2dyYXBoaWNMb2NhdGlvbi5jb3VudHJ5KTtcbiAgICB0aGlzLmNvbnRyb2xzLmFkZHJlc3Muc2V0VmFsdWUoZ2VvZ3JhcGhpY0xvY2F0aW9uLmFkZHJlc3MpO1xuICAgIGlmIChnZW9ncmFwaGljTG9jYXRpb24uY29vcmRpbmF0ZXMpIHtcbiAgICAgIHRoaXMuY29udHJvbHMubGF0LnNldFZhbHVlKGdlb2dyYXBoaWNMb2NhdGlvbi5jb29yZGluYXRlcy5sYXQpO1xuICAgICAgdGhpcy5jb250cm9scy5sb24uc2V0VmFsdWUoZ2VvZ3JhcGhpY0xvY2F0aW9uLmNvb3JkaW5hdGVzLmxvbik7XG4gICAgfVxuICB9XG5cbiAgLyoqIEZvcm0gY29udHJvbHMgc2F2ZXIgaW50byBhIGdlb2dyYXBoaWMgbG9jYXRpb24uICovXG4gIHNhdmVDb250cm9scygpOiBHZW9ncmFwaGljTG9jYXRpb24ge1xuICAgIHJldHVybiB7XG4gICAgICBjaXR5ICAgICAgIDogdGhpcy5jb250cm9scy5jaXR5LnZhbHVlLFxuICAgICAgZGVwYXJ0bWVudCA6IHRoaXMuY29udHJvbHMuZGVwYXJ0bWVudC52YWx1ZSxcbiAgICAgIHJlZ2lvbiAgICAgOiB0aGlzLmNvbnRyb2xzLnJlZ2lvbi52YWx1ZSxcbiAgICAgIGNvdW50cnkgICAgOiB0aGlzLmNvbnRyb2xzLmNvdW50cnkudmFsdWUsXG4gICAgICBhZGRyZXNzICAgIDogdGhpcy5jb250cm9scy5hZGRyZXNzLnZhbHVlLFxuICAgICAgY29vcmRpbmF0ZXM6IHtcbiAgICAgICAgbGF0OiB0aGlzLmNvbnRyb2xzLmxhdC52YWx1ZSxcbiAgICAgICAgbG9uOiB0aGlzLmNvbnRyb2xzLmxvbi52YWx1ZVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG59XG4iXX0=