/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function BootstartLocationFormOptions() { }
/**
 * Question placeholder
 * @type {?|undefined}
 */
BootstartLocationFormOptions.prototype.label;
/**
 * Optional question icon (Angular Material icon)
 * @type {?|undefined}
 */
BootstartLocationFormOptions.prototype.icon;
/**
 * Is the question required?
 * @type {?|undefined}
 */
BootstartLocationFormOptions.prototype.required;
/**
 * Question field's width in percent
 * @type {?|undefined}
 */
BootstartLocationFormOptions.prototype.width;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0tb3B0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Jvb3RzdGFydC1nZW9ncmFwaHkvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzL2Zvcm0vYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0tb3B0aW9ucy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBCb290c3RhcnRMb2NhdGlvbkZvcm1PcHRpb25zIHtcblxuICAvKiogUXVlc3Rpb24gcGxhY2Vob2xkZXIgKi9cbiAgbGFiZWw/OiBzdHJpbmc7XG5cbiAgLyoqIE9wdGlvbmFsIHF1ZXN0aW9uIGljb24gKEFuZ3VsYXIgTWF0ZXJpYWwgaWNvbikgKi9cbiAgaWNvbj86IHN0cmluZztcblxuICAvKiogSXMgdGhlIHF1ZXN0aW9uIHJlcXVpcmVkPyAqL1xuICByZXF1aXJlZD86IGJvb2xlYW47XG5cbiAgLyoqIFF1ZXN0aW9uIGZpZWxkJ3Mgd2lkdGggaW4gcGVyY2VudCAqL1xuICB3aWR0aD86IG51bWJlcjtcblxufVxuIl19