/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { BootstartLocationFormComponent } from './components/bootstart-location-form/bootstart-location-form.component';
import { MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { GooglePlacesDirective } from './components/bootstart-location-form/google-places/google-places.directive';
var BootstartGeographyModule = /** @class */ (function () {
    function BootstartGeographyModule() {
    }
    BootstartGeographyModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        FormsModule,
                        ReactiveFormsModule,
                        MatFormFieldModule,
                        MatInputModule,
                        MatIconModule,
                        TranslateModule
                    ],
                    declarations: [
                        BootstartLocationFormComponent,
                        GooglePlacesDirective
                    ],
                    exports: [
                        BootstartLocationFormComponent,
                        GooglePlacesDirective
                    ],
                    providers: [],
                },] },
    ];
    return BootstartGeographyModule;
}());
export { BootstartGeographyModule };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWdlb2dyYXBoeS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZ2VvZ3JhcGh5LyIsInNvdXJjZXMiOlsibGliL2Jvb3RzdGFydC1nZW9ncmFwaHkubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3ZDLE9BQU8sRUFBQyw4QkFBOEIsRUFBQyxNQUFNLHdFQUF3RSxDQUFDO0FBQ3RILE9BQU8sRUFBQyxrQkFBa0IsRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFDLE1BQU0sbUJBQW1CLENBQUM7QUFDcEYsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBQyxXQUFXLEVBQUUsbUJBQW1CLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQztBQUNoRSxPQUFPLEVBQUMsZUFBZSxFQUFDLE1BQU0scUJBQXFCLENBQUM7QUFDcEQsT0FBTyxFQUFDLHFCQUFxQixFQUFDLE1BQU0sNEVBQTRFLENBQUM7Ozs7O2dCQUVoSCxRQUFRLFNBQUM7b0JBQ0UsT0FBTyxFQUFPO3dCQUNaLFlBQVk7d0JBQ1osV0FBVzt3QkFDWCxtQkFBbUI7d0JBQ25CLGtCQUFrQjt3QkFDbEIsY0FBYzt3QkFDZCxhQUFhO3dCQUNiLGVBQWU7cUJBQ2hCO29CQUNELFlBQVksRUFBRTt3QkFDWiw4QkFBOEI7d0JBQzlCLHFCQUFxQjtxQkFDdEI7b0JBQ0QsT0FBTyxFQUFPO3dCQUNaLDhCQUE4Qjt3QkFDOUIscUJBQXFCO3FCQUN0QjtvQkFDRCxTQUFTLEVBQUssRUFBRTtpQkFDakI7O21DQTNCWDs7U0E0QmEsd0JBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOZ01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Jvb3RzdGFydExvY2F0aW9uRm9ybUNvbXBvbmVudH0gZnJvbSAnLi9jb21wb25lbnRzL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtLmNvbXBvbmVudCc7XG5pbXBvcnQge01hdEZvcm1GaWVsZE1vZHVsZSwgTWF0SWNvbk1vZHVsZSwgTWF0SW5wdXRNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZX0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHtUcmFuc2xhdGVNb2R1bGV9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuaW1wb3J0IHtHb29nbGVQbGFjZXNEaXJlY3RpdmV9IGZyb20gJy4vY29tcG9uZW50cy9ib290c3RhcnQtbG9jYXRpb24tZm9ybS9nb29nbGUtcGxhY2VzL2dvb2dsZS1wbGFjZXMuZGlyZWN0aXZlJztcblxuQE5nTW9kdWxlKHtcbiAgICAgICAgICAgIGltcG9ydHMgICAgIDogW1xuICAgICAgICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgICAgICAgIEZvcm1zTW9kdWxlLFxuICAgICAgICAgICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgICAgICAgICAgICBNYXRGb3JtRmllbGRNb2R1bGUsXG4gICAgICAgICAgICAgIE1hdElucHV0TW9kdWxlLFxuICAgICAgICAgICAgICBNYXRJY29uTW9kdWxlLFxuICAgICAgICAgICAgICBUcmFuc2xhdGVNb2R1bGVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0TG9jYXRpb25Gb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBHb29nbGVQbGFjZXNEaXJlY3RpdmVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBleHBvcnRzICAgICA6IFtcbiAgICAgICAgICAgICAgQm9vdHN0YXJ0TG9jYXRpb25Gb3JtQ29tcG9uZW50LFxuICAgICAgICAgICAgICBHb29nbGVQbGFjZXNEaXJlY3RpdmVcbiAgICAgICAgICAgIF0sXG4gICAgICAgICAgICBwcm92aWRlcnMgICA6IFtdLFxuICAgICAgICAgIH0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0R2VvZ3JhcGh5TW9kdWxlIHtcbn1cbiJdfQ==