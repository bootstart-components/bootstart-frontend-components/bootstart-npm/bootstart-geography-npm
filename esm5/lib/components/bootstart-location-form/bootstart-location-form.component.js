/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { BootstartLocationFormGroup } from '../../models/form/bootstart-location-form-group';
var BootstartLocationFormComponent = /** @class */ (function () {
    function BootstartLocationFormComponent() {
    }
    /**
     * @return {?}
     */
    BootstartLocationFormComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._setDefaultOptions();
    };
    /**
     * @return {?}
     */
    BootstartLocationFormComponent.prototype._setDefaultOptions = /**
     * @return {?}
     */
    function () {
        if (this.options.width === undefined) {
            this.options.width = 100;
        }
    };
    /** Place update */
    /**
     * Place update
     * @param {?} place
     * @return {?}
     */
    BootstartLocationFormComponent.prototype.updatePlace = /**
     * Place update
     * @param {?} place
     * @return {?}
     */
    function (place) {
        var _this = this;
        this.group.controls.city.setValue('');
        this.group.controls.department.setValue('');
        this.group.controls.region.setValue('');
        this.group.controls.country.setValue('');
        place.address_components.forEach(function (address_component) {
            if (address_component.types.indexOf('locality') >= 0) {
                _this.group.controls.city.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('administrative_area_level_2') >= 0) {
                _this.group.controls.department.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('administrative_area_level_1') >= 0) {
                _this.group.controls.region.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('country') >= 0) {
                _this.group.controls.country.setValue(address_component.long_name);
            }
        });
        this.group.controls.lat.setValue(place.geometry.location.lat());
        this.group.controls.lon.setValue(place.geometry.location.lng());
        this.group.controls.address.setValue(place.formatted_address);
    };
    BootstartLocationFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-location-form',
                    template: "<mat-form-field [ngStyle]=\"{'width' : options.width+'%'}\">\n  <mat-label>\n    <mat-icon *ngIf=\"options.icon\">{{options.icon}}</mat-icon>\n    <span *ngIf=\"options.icon\">&nbsp;</span>\n    <span *ngIf=\"options.label\">{{ options.label | translate}}</span>\n  </mat-label>\n\n  <input matInput appGooglePlaces\n         type=\"text\"\n         [placeholder]=\"this.group.controls.address.value\"\n         (selectionChange)=\"updatePlace($event)\"\n         [required]=\"options.required\">\n</mat-form-field>\n"
                },] },
    ];
    /** @nocollapse */
    BootstartLocationFormComponent.ctorParameters = function () { return []; };
    BootstartLocationFormComponent.propDecorators = {
        group: [{ type: Input }],
        options: [{ type: Input }]
    };
    return BootstartLocationFormComponent;
}());
export { BootstartLocationFormComponent };
if (false) {
    /**
     * Associated form group
     * @type {?}
     */
    BootstartLocationFormComponent.prototype.group;
    /**
     * Set of options
     * @type {?}
     */
    BootstartLocationFormComponent.prototype.options;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWdlb2dyYXBoeS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtL2Jvb3RzdGFydC1sb2NhdGlvbi1mb3JtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFFdkQsT0FBTyxFQUFDLDBCQUEwQixFQUFDLE1BQU0saURBQWlELENBQUM7O0lBNEJ6RjtLQUNDOzs7O0lBRUQsaURBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7S0FDM0I7Ozs7SUFFTywyREFBa0I7Ozs7UUFDeEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7U0FDMUI7O0lBR0gsbUJBQW1COzs7Ozs7SUFDbkIsb0RBQVc7Ozs7O0lBQVgsVUFBWSxLQUFxQztRQUFqRCxpQkF3QkM7UUF2QkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUV6QyxLQUFLLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFVBQUEsaUJBQWlCO1lBQ2hELEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckQsS0FBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNoRTtZQUNELEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4RSxLQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3RFO1lBQ0QsRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDbEU7WUFDRCxFQUFFLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BELEtBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDbkU7U0FDRixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ2hFLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7S0FDL0Q7O2dCQS9ERixTQUFTLFNBQUM7b0JBQ0UsUUFBUSxFQUFLLHlCQUF5QjtvQkFDdEMsUUFBUSxFQUFFLHVnQkFhdEI7aUJBQ1c7Ozs7O3dCQUlULEtBQUs7MEJBR0wsS0FBSzs7eUNBNUJSOztTQXNCYSw4QkFBOEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXQsIE9uSW5pdH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge2dvb2dsZX0gZnJvbSAnZ29vZ2xlLW1hcHMnO1xuaW1wb3J0IHtCb290c3RhcnRMb2NhdGlvbkZvcm1Hcm91cH0gZnJvbSAnLi4vLi4vbW9kZWxzL2Zvcm0vYm9vdHN0YXJ0LWxvY2F0aW9uLWZvcm0tZ3JvdXAnO1xuaW1wb3J0IHtCb290c3RhcnRMb2NhdGlvbkZvcm1PcHRpb25zfSBmcm9tICcuLi8uLi9tb2RlbHMvZm9ybS9ib290c3RhcnQtbG9jYXRpb24tZm9ybS1vcHRpb25zJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtbG9jYXRpb24tZm9ybScsXG4gICAgICAgICAgICAgdGVtcGxhdGU6IGA8bWF0LWZvcm0tZmllbGQgW25nU3R5bGVdPVwieyd3aWR0aCcgOiBvcHRpb25zLndpZHRoKyclJ31cIj5cbiAgPG1hdC1sYWJlbD5cbiAgICA8bWF0LWljb24gKm5nSWY9XCJvcHRpb25zLmljb25cIj57e29wdGlvbnMuaWNvbn19PC9tYXQtaWNvbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMuaWNvblwiPiZuYnNwOzwvc3Bhbj5cbiAgICA8c3BhbiAqbmdJZj1cIm9wdGlvbnMubGFiZWxcIj57eyBvcHRpb25zLmxhYmVsIHwgdHJhbnNsYXRlfX08L3NwYW4+XG4gIDwvbWF0LWxhYmVsPlxuXG4gIDxpbnB1dCBtYXRJbnB1dCBhcHBHb29nbGVQbGFjZXNcbiAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgIFtwbGFjZWhvbGRlcl09XCJ0aGlzLmdyb3VwLmNvbnRyb2xzLmFkZHJlc3MudmFsdWVcIlxuICAgICAgICAgKHNlbGVjdGlvbkNoYW5nZSk9XCJ1cGRhdGVQbGFjZSgkZXZlbnQpXCJcbiAgICAgICAgIFtyZXF1aXJlZF09XCJvcHRpb25zLnJlcXVpcmVkXCI+XG48L21hdC1mb3JtLWZpZWxkPlxuYFxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydExvY2F0aW9uRm9ybUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgLyoqIEFzc29jaWF0ZWQgZm9ybSBncm91cCAqL1xuICBASW5wdXQoKSBncm91cDogQm9vdHN0YXJ0TG9jYXRpb25Gb3JtR3JvdXA7XG5cbiAgLyoqIFNldCBvZiBvcHRpb25zICovXG4gIEBJbnB1dCgpIG9wdGlvbnM6IEJvb3RzdGFydExvY2F0aW9uRm9ybU9wdGlvbnM7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLl9zZXREZWZhdWx0T3B0aW9ucygpO1xuICB9XG5cbiAgcHJpdmF0ZSBfc2V0RGVmYXVsdE9wdGlvbnMoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9ucy53aWR0aCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICB0aGlzLm9wdGlvbnMud2lkdGggPSAxMDA7XG4gICAgfVxuICB9XG5cbiAgLyoqIFBsYWNlIHVwZGF0ZSAqL1xuICB1cGRhdGVQbGFjZShwbGFjZTogZ29vZ2xlLm1hcHMucGxhY2VzLlBsYWNlUmVzdWx0KSB7XG4gICAgdGhpcy5ncm91cC5jb250cm9scy5jaXR5LnNldFZhbHVlKCcnKTtcbiAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmRlcGFydG1lbnQuc2V0VmFsdWUoJycpO1xuICAgIHRoaXMuZ3JvdXAuY29udHJvbHMucmVnaW9uLnNldFZhbHVlKCcnKTtcbiAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmNvdW50cnkuc2V0VmFsdWUoJycpO1xuXG4gICAgcGxhY2UuYWRkcmVzc19jb21wb25lbnRzLmZvckVhY2goYWRkcmVzc19jb21wb25lbnQgPT4ge1xuICAgICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzLmluZGV4T2YoJ2xvY2FsaXR5JykgPj0gMCkge1xuICAgICAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmNpdHkuc2V0VmFsdWUoYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lKTtcbiAgICAgIH1cbiAgICAgIGlmIChhZGRyZXNzX2NvbXBvbmVudC50eXBlcy5pbmRleE9mKCdhZG1pbmlzdHJhdGl2ZV9hcmVhX2xldmVsXzInKSA+PSAwKSB7XG4gICAgICAgIHRoaXMuZ3JvdXAuY29udHJvbHMuZGVwYXJ0bWVudC5zZXRWYWx1ZShhZGRyZXNzX2NvbXBvbmVudC5sb25nX25hbWUpO1xuICAgICAgfVxuICAgICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzLmluZGV4T2YoJ2FkbWluaXN0cmF0aXZlX2FyZWFfbGV2ZWxfMScpID49IDApIHtcbiAgICAgICAgdGhpcy5ncm91cC5jb250cm9scy5yZWdpb24uc2V0VmFsdWUoYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lKTtcbiAgICAgIH1cbiAgICAgIGlmIChhZGRyZXNzX2NvbXBvbmVudC50eXBlcy5pbmRleE9mKCdjb3VudHJ5JykgPj0gMCkge1xuICAgICAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmNvdW50cnkuc2V0VmFsdWUoYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHRoaXMuZ3JvdXAuY29udHJvbHMubGF0LnNldFZhbHVlKHBsYWNlLmdlb21ldHJ5LmxvY2F0aW9uLmxhdCgpKTtcbiAgICB0aGlzLmdyb3VwLmNvbnRyb2xzLmxvbi5zZXRWYWx1ZShwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKSk7XG4gICAgdGhpcy5ncm91cC5jb250cm9scy5hZGRyZXNzLnNldFZhbHVlKHBsYWNlLmZvcm1hdHRlZF9hZGRyZXNzKTtcbiAgfVxuXG59XG4iXX0=