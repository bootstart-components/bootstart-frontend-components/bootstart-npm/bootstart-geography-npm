/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
import { BootstartGeographyFormGroup } from '../../models/bootstart-geography-form-group';
var BootstartGeographyQuestionComponent = /** @class */ (function () {
    function BootstartGeographyQuestionComponent() {
        /**
         * Is the question required?
         */
        this.required = false;
        /**
         * Is the question displayed with a width of 100%? True by default.
         */
        this.fullWidth = false;
    }
    /**
     * @param {?} place
     * @return {?}
     */
    BootstartGeographyQuestionComponent.prototype.updatePlace = /**
     * @param {?} place
     * @return {?}
     */
    function (place) {
        var _this = this;
        this.formGroup.controls.city.setValue('');
        this.formGroup.controls.department.setValue('');
        this.formGroup.controls.region.setValue('');
        this.formGroup.controls.country.setValue('');
        place.address_components.forEach(function (address_component) {
            if (address_component.types.indexOf('locality') >= 0) {
                _this.formGroup.controls.city.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('administrative_area_level_2') >= 0) {
                _this.formGroup.controls.department.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('administrative_area_level_1') >= 0) {
                _this.formGroup.controls.region.setValue(address_component.long_name);
            }
            if (address_component.types.indexOf('country') >= 0) {
                _this.formGroup.controls.country.setValue(address_component.long_name);
            }
        });
        this.formGroup.controls.lat.setValue(place.geometry.location.lat());
        this.formGroup.controls.lon.setValue(place.geometry.location.lng());
        this.formGroup.controls.address.setValue(place.formatted_address);
    };
    /**
     * @return {?}
     */
    BootstartGeographyQuestionComponent.prototype.getPlaceholder = /**
     * @return {?}
     */
    function () {
        return ((this.formGroup.controls.address.value === null || this.formGroup.controls.address.value === '') ?
            this.questionPlaceholder : this.formGroup.controls.address.value);
    };
    BootstartGeographyQuestionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'bootstart-geography-question',
                    template: "<mat-form-field [ngClass]=\"{'full-width': fullWidth}\" floatLabel=\"never\">\n  <mat-label>\n    <mat-icon *ngIf=\"questionIcon\">{{questionIcon}}</mat-icon>\n    <span *ngIf=\"questionIcon\">&nbsp;</span>\n    <span *ngIf=\"questionLabel\">{{ questionLabel | translate}}</span>\n  </mat-label>\n\n  <input matInput appGooglePlaces\n         type=\"text\"\n         [placeholder]=\"getPlaceholder()\"\n         (selectionChange)=\"updatePlace($event)\"\n         [required]=\"required\">\n</mat-form-field>\n"
                },] },
    ];
    /** @nocollapse */
    BootstartGeographyQuestionComponent.ctorParameters = function () { return []; };
    BootstartGeographyQuestionComponent.propDecorators = {
        formGroup: [{ type: Input }],
        questionLabel: [{ type: Input }],
        questionIcon: [{ type: Input }],
        questionPlaceholder: [{ type: Input }],
        required: [{ type: Input }],
        fullWidth: [{ type: Input }]
    };
    return BootstartGeographyQuestionComponent;
}());
export { BootstartGeographyQuestionComponent };
if (false) {
    /**
     * Form group
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.formGroup;
    /**
     * Optional question label. Translation is applied.
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.questionLabel;
    /**
     * Optional question icon (Angular Material icon)
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.questionIcon;
    /**
     * Optional placeholder
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.questionPlaceholder;
    /**
     * Is the question required?
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.required;
    /**
     * Is the question displayed with a width of 100%? True by default.
     * @type {?}
     */
    BootstartGeographyQuestionComponent.prototype.fullWidth;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWdlb2dyYXBoeS1xdWVzdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZ2VvZ3JhcGh5LyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWdlb2dyYXBoeS1xdWVzdGlvbi9ib290c3RhcnQtZ2VvZ3JhcGh5LXF1ZXN0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxLQUFLLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFFL0MsT0FBTyxFQUFDLDJCQUEyQixFQUFDLE1BQU0sNkNBQTZDLENBQUM7O0lBaUN0Rjs7Ozt3QkFKb0IsS0FBSzs7Ozt5QkFFSixLQUFLO0tBR3pCOzs7OztJQUdELHlEQUFXOzs7O0lBQVgsVUFBWSxLQUFxQztRQUFqRCxpQkF3QkM7UUF2QkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUU3QyxLQUFLLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFVBQUEsaUJBQWlCO1lBQ2hELEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckQsS0FBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNwRTtZQUNELEVBQUUsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsNkJBQTZCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4RSxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQzFFO1lBQ0QsRUFBRSxDQUFDLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLEtBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDdEU7WUFDRCxFQUFFLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3BELEtBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDdkU7U0FDRixDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7S0FDbkU7Ozs7SUFFRCw0REFBYzs7O0lBQWQ7UUFDRSxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNsRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUMzRTs7Z0JBaEVGLFNBQVMsU0FBQztvQkFDRSxRQUFRLEVBQUssOEJBQThCO29CQUMzQyxRQUFRLEVBQUUsK2ZBYXRCO2lCQUNXOzs7Ozs0QkFHVCxLQUFLO2dDQUVMLEtBQUs7K0JBRUwsS0FBSztzQ0FFTCxLQUFLOzJCQUVMLEtBQUs7NEJBRUwsS0FBSzs7OENBakNSOztTQXFCYSxtQ0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgSW5wdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtnb29nbGV9IGZyb20gJ2dvb2dsZS1tYXBzJztcbmltcG9ydCB7Qm9vdHN0YXJ0R2VvZ3JhcGh5Rm9ybUdyb3VwfSBmcm9tICcuLi8uLi9tb2RlbHMvYm9vdHN0YXJ0LWdlb2dyYXBoeS1mb3JtLWdyb3VwJztcblxuQENvbXBvbmVudCh7XG4gICAgICAgICAgICAgc2VsZWN0b3IgICA6ICdib290c3RhcnQtZ2VvZ3JhcGh5LXF1ZXN0aW9uJyxcbiAgICAgICAgICAgICB0ZW1wbGF0ZTogYDxtYXQtZm9ybS1maWVsZCBbbmdDbGFzc109XCJ7J2Z1bGwtd2lkdGgnOiBmdWxsV2lkdGh9XCIgZmxvYXRMYWJlbD1cIm5ldmVyXCI+XG4gIDxtYXQtbGFiZWw+XG4gICAgPG1hdC1pY29uICpuZ0lmPVwicXVlc3Rpb25JY29uXCI+e3txdWVzdGlvbkljb259fTwvbWF0LWljb24+XG4gICAgPHNwYW4gKm5nSWY9XCJxdWVzdGlvbkljb25cIj4mbmJzcDs8L3NwYW4+XG4gICAgPHNwYW4gKm5nSWY9XCJxdWVzdGlvbkxhYmVsXCI+e3sgcXVlc3Rpb25MYWJlbCB8IHRyYW5zbGF0ZX19PC9zcGFuPlxuICA8L21hdC1sYWJlbD5cblxuICA8aW5wdXQgbWF0SW5wdXQgYXBwR29vZ2xlUGxhY2VzXG4gICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICBbcGxhY2Vob2xkZXJdPVwiZ2V0UGxhY2Vob2xkZXIoKVwiXG4gICAgICAgICAoc2VsZWN0aW9uQ2hhbmdlKT1cInVwZGF0ZVBsYWNlKCRldmVudClcIlxuICAgICAgICAgW3JlcXVpcmVkXT1cInJlcXVpcmVkXCI+XG48L21hdC1mb3JtLWZpZWxkPlxuYFxuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEJvb3RzdGFydEdlb2dyYXBoeVF1ZXN0aW9uQ29tcG9uZW50IHtcbiAgLyoqIEZvcm0gZ3JvdXAgKi9cbiAgQElucHV0KCkgZm9ybUdyb3VwOiBCb290c3RhcnRHZW9ncmFwaHlGb3JtR3JvdXA7XG4gIC8qKiBPcHRpb25hbCBxdWVzdGlvbiBsYWJlbC4gVHJhbnNsYXRpb24gaXMgYXBwbGllZC4gKi9cbiAgQElucHV0KCkgcXVlc3Rpb25MYWJlbDogc3RyaW5nO1xuICAvKiogT3B0aW9uYWwgcXVlc3Rpb24gaWNvbiAoQW5ndWxhciBNYXRlcmlhbCBpY29uKSAqL1xuICBASW5wdXQoKSBxdWVzdGlvbkljb246IHN0cmluZztcbiAgLyoqIE9wdGlvbmFsIHBsYWNlaG9sZGVyICovXG4gIEBJbnB1dCgpIHF1ZXN0aW9uUGxhY2Vob2xkZXI6IHN0cmluZztcbiAgLyoqIElzIHRoZSBxdWVzdGlvbiByZXF1aXJlZD8gKi9cbiAgQElucHV0KCkgcmVxdWlyZWQgPSBmYWxzZTtcbiAgLyoqIElzIHRoZSBxdWVzdGlvbiBkaXNwbGF5ZWQgd2l0aCBhIHdpZHRoIG9mIDEwMCU/IFRydWUgYnkgZGVmYXVsdC4gKi9cbiAgQElucHV0KCkgZnVsbFdpZHRoID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gIH1cblxuXG4gIHVwZGF0ZVBsYWNlKHBsYWNlOiBnb29nbGUubWFwcy5wbGFjZXMuUGxhY2VSZXN1bHQpIHtcbiAgICB0aGlzLmZvcm1Hcm91cC5jb250cm9scy5jaXR5LnNldFZhbHVlKCcnKTtcbiAgICB0aGlzLmZvcm1Hcm91cC5jb250cm9scy5kZXBhcnRtZW50LnNldFZhbHVlKCcnKTtcbiAgICB0aGlzLmZvcm1Hcm91cC5jb250cm9scy5yZWdpb24uc2V0VmFsdWUoJycpO1xuICAgIHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLmNvdW50cnkuc2V0VmFsdWUoJycpO1xuXG4gICAgcGxhY2UuYWRkcmVzc19jb21wb25lbnRzLmZvckVhY2goYWRkcmVzc19jb21wb25lbnQgPT4ge1xuICAgICAgaWYgKGFkZHJlc3NfY29tcG9uZW50LnR5cGVzLmluZGV4T2YoJ2xvY2FsaXR5JykgPj0gMCkge1xuICAgICAgICB0aGlzLmZvcm1Hcm91cC5jb250cm9scy5jaXR5LnNldFZhbHVlKGFkZHJlc3NfY29tcG9uZW50LmxvbmdfbmFtZSk7XG4gICAgICB9XG4gICAgICBpZiAoYWRkcmVzc19jb21wb25lbnQudHlwZXMuaW5kZXhPZignYWRtaW5pc3RyYXRpdmVfYXJlYV9sZXZlbF8yJykgPj0gMCkge1xuICAgICAgICB0aGlzLmZvcm1Hcm91cC5jb250cm9scy5kZXBhcnRtZW50LnNldFZhbHVlKGFkZHJlc3NfY29tcG9uZW50LmxvbmdfbmFtZSk7XG4gICAgICB9XG4gICAgICBpZiAoYWRkcmVzc19jb21wb25lbnQudHlwZXMuaW5kZXhPZignYWRtaW5pc3RyYXRpdmVfYXJlYV9sZXZlbF8xJykgPj0gMCkge1xuICAgICAgICB0aGlzLmZvcm1Hcm91cC5jb250cm9scy5yZWdpb24uc2V0VmFsdWUoYWRkcmVzc19jb21wb25lbnQubG9uZ19uYW1lKTtcbiAgICAgIH1cbiAgICAgIGlmIChhZGRyZXNzX2NvbXBvbmVudC50eXBlcy5pbmRleE9mKCdjb3VudHJ5JykgPj0gMCkge1xuICAgICAgICB0aGlzLmZvcm1Hcm91cC5jb250cm9scy5jb3VudHJ5LnNldFZhbHVlKGFkZHJlc3NfY29tcG9uZW50LmxvbmdfbmFtZSk7XG4gICAgICB9XG4gICAgfSk7XG5cbiAgICB0aGlzLmZvcm1Hcm91cC5jb250cm9scy5sYXQuc2V0VmFsdWUocGxhY2UuZ2VvbWV0cnkubG9jYXRpb24ubGF0KCkpO1xuICAgIHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLmxvbi5zZXRWYWx1ZShwbGFjZS5nZW9tZXRyeS5sb2NhdGlvbi5sbmcoKSk7XG4gICAgdGhpcy5mb3JtR3JvdXAuY29udHJvbHMuYWRkcmVzcy5zZXRWYWx1ZShwbGFjZS5mb3JtYXR0ZWRfYWRkcmVzcyk7XG4gIH1cblxuICBnZXRQbGFjZWhvbGRlcigpOiBzdHJpbmcge1xuICAgIHJldHVybiAoKHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLmFkZHJlc3MudmFsdWUgPT09IG51bGwgfHwgdGhpcy5mb3JtR3JvdXAuY29udHJvbHMuYWRkcmVzcy52YWx1ZSA9PT0gJycpID9cbiAgICAgICAgICAgIHRoaXMucXVlc3Rpb25QbGFjZWhvbGRlciA6IHRoaXMuZm9ybUdyb3VwLmNvbnRyb2xzLmFkZHJlc3MudmFsdWUpO1xuICB9XG59XG4iXX0=