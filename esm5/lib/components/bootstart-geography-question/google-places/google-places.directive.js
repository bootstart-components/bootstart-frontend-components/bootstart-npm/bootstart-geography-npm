/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Directive, ElementRef, EventEmitter, Output } from '@angular/core';
var GooglePlacesDirective = /** @class */ (function () {
    function GooglePlacesDirective(elRef) {
        this.elRef = elRef;
        this.selectionChange = new EventEmitter();
        this.element = elRef.nativeElement;
    }
    /**
     * @return {?}
     */
    GooglePlacesDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var autocomplete = new google.maps.places.Autocomplete(this.element);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            _this.selectionChange.emit(autocomplete.getPlace());
        });
    };
    GooglePlacesDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[appGooglePlaces]'
                },] },
    ];
    /** @nocollapse */
    GooglePlacesDirective.ctorParameters = function () { return [
        { type: ElementRef }
    ]; };
    GooglePlacesDirective.propDecorators = {
        selectionChange: [{ type: Output }]
    };
    return GooglePlacesDirective;
}());
export { GooglePlacesDirective };
if (false) {
    /** @type {?} */
    GooglePlacesDirective.prototype.selectionChange;
    /** @type {?} */
    GooglePlacesDirective.prototype.element;
    /** @type {?} */
    GooglePlacesDirective.prototype.elRef;
}

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlLXBsYWNlcy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZ2VvZ3JhcGh5LyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvYm9vdHN0YXJ0LWdlb2dyYXBoeS1xdWVzdGlvbi9nb29nbGUtcGxhY2VzL2dvb2dsZS1wbGFjZXMuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQVUsTUFBTSxFQUFDLE1BQU0sZUFBZSxDQUFDOztJQVVoRiwrQkFBb0IsS0FBaUI7UUFBakIsVUFBSyxHQUFMLEtBQUssQ0FBWTsrQkFIcUMsSUFBSSxZQUFZLEVBQUU7UUFJMUYsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsYUFBYSxDQUFDO0tBQ3BDOzs7O0lBRUQsd0NBQVE7OztJQUFSO1FBQUEsaUJBS0M7O1FBSkMsSUFBTSxZQUFZLEdBQUcsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3ZFLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsZUFBZSxFQUFFO1lBQzNELEtBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1NBQ3BELENBQUMsQ0FBQztLQUNKOztnQkFoQkYsU0FBUyxTQUFDO29CQUNFLFFBQVEsRUFBRSxtQkFBbUI7aUJBQzlCOzs7O2dCQUxPLFVBQVU7OztrQ0FPMUIsTUFBTTs7Z0NBUFQ7O1NBTWEscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgT25Jbml0LCBPdXRwdXR9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgZ29vZ2xlIH0gZnJvbSAnZ29vZ2xlLW1hcHMnO1xuXG5ARGlyZWN0aXZlKHtcbiAgICAgICAgICAgICBzZWxlY3RvcjogJ1thcHBHb29nbGVQbGFjZXNdJ1xuICAgICAgICAgICB9KVxuZXhwb3J0IGNsYXNzIEdvb2dsZVBsYWNlc0RpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBPdXRwdXQoKSBzZWxlY3Rpb25DaGFuZ2U6IEV2ZW50RW1pdHRlcjxnb29nbGUubWFwcy5wbGFjZXMuUGxhY2VSZXN1bHQ+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBwcml2YXRlIGVsZW1lbnQ6IEhUTUxJbnB1dEVsZW1lbnQ7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZikge1xuICAgIHRoaXMuZWxlbWVudCA9IGVsUmVmLm5hdGl2ZUVsZW1lbnQ7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBjb25zdCBhdXRvY29tcGxldGUgPSBuZXcgZ29vZ2xlLm1hcHMucGxhY2VzLkF1dG9jb21wbGV0ZSh0aGlzLmVsZW1lbnQpO1xuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZExpc3RlbmVyKGF1dG9jb21wbGV0ZSwgJ3BsYWNlX2NoYW5nZWQnLCAoKSA9PiB7XG4gICAgICB0aGlzLnNlbGVjdGlvbkNoYW5nZS5lbWl0KGF1dG9jb21wbGV0ZS5nZXRQbGFjZSgpKTtcbiAgICB9KTtcbiAgfVxuXG59XG4iXX0=