/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
var BootstartGeographyComponent = /** @class */ (function () {
    function BootstartGeographyComponent() {
    }
    /**
     * @return {?}
     */
    BootstartGeographyComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    BootstartGeographyComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-bootstart-geography',
                    template: "\n    <p>\n      bootstart-geography works!\n    </p>\n  ",
                    styles: []
                },] },
    ];
    /** @nocollapse */
    BootstartGeographyComponent.ctorParameters = function () { return []; };
    return BootstartGeographyComponent;
}());
export { BootstartGeographyComponent };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWdlb2dyYXBoeS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ib290c3RhcnQtZ2VvZ3JhcGh5LyIsInNvdXJjZXMiOlsibGliL2Jvb3RzdGFydC1nZW9ncmFwaHkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDOztJQWFoRDtLQUFpQjs7OztJQUVqQiw4Q0FBUTs7O0lBQVI7S0FDQzs7Z0JBZEYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSx5QkFBeUI7b0JBQ25DLFFBQVEsRUFBRSwyREFJVDtvQkFDRCxNQUFNLEVBQUUsRUFBRTtpQkFDWDs7OztzQ0FWRDs7U0FXYSwyQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdsaWItYm9vdHN0YXJ0LWdlb2dyYXBoeScsXG4gIHRlbXBsYXRlOiBgXG4gICAgPHA+XG4gICAgICBib290c3RhcnQtZ2VvZ3JhcGh5IHdvcmtzIVxuICAgIDwvcD5cbiAgYCxcbiAgc3R5bGVzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBCb290c3RhcnRHZW9ncmFwaHlDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxufVxuIl19