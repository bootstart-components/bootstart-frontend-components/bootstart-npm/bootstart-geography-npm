/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var BootstartGeographyService = /** @class */ (function () {
    function BootstartGeographyService() {
    }
    BootstartGeographyService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] },
    ];
    /** @nocollapse */
    BootstartGeographyService.ctorParameters = function () { return []; };
    /** @nocollapse */ BootstartGeographyService.ngInjectableDef = i0.defineInjectable({ factory: function BootstartGeographyService_Factory() { return new BootstartGeographyService(); }, token: BootstartGeographyService, providedIn: "root" });
    return BootstartGeographyService;
}());
export { BootstartGeographyService };

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vdHN0YXJ0LWdlb2dyYXBoeS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYm9vdHN0YXJ0LWdlb2dyYXBoeS8iLCJzb3VyY2VzIjpbImxpYi9ib290c3RhcnQtZ2VvZ3JhcGh5LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7OztJQU96QztLQUFpQjs7Z0JBTGxCLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7O29DQUpEOztTQUthLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgQm9vdHN0YXJ0R2VvZ3JhcGh5U2VydmljZSB7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cbn1cbiJdfQ==